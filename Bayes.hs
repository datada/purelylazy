module Bayes where

import Data.Ratio
import Control.Exception (assert)

-- "dividing by p(D)" by normalizing instead of calculating P(D) using total probablility
normalize :: (Eq a) => [(a, Rational)] -> [(a, Rational)]
normalize xs = let total = sum (map (\(_, p) -> p) xs)
               in map (\(x, p) -> (x, p / total)) xs

-- element by element *
-- each H is updated with P(D|H)
-- assert x == y ie H's are aligned
mult :: (Eq a) => [(a, Rational)] -> [(a, Rational)] -> [(a, Rational)]
mult xs ys = zipWith (\(x, p) (y, q) -> assert (x == y) (x, p * q)) xs ys


posterior :: (Eq a, Eq b) => [(a, Rational)] -> (b -> [(a, Rational)]) -> b -> [(a, Rational)]
posterior prior likely evidence = normalize (mult prior 
	                                              (likely evidence))


