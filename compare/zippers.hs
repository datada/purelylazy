data Tree a = Empty | Node a (Tree a) (Tree a) deriving (Show)  

freeTree :: Tree Char  
freeTree =   
    Node 'P'  
        (Node 'O'  
            (Node 'L'  
                (Node 'N' Empty Empty)  
                (Node 'T' Empty Empty))  
            (Node 'Y'  
                (Node 'S' Empty Empty)  
                (Node 'A' Empty Empty)))  
        (Node 'L'  
            (Node 'W'  
                (Node 'C' Empty Empty)  
                (Node 'R' Empty Empty))  
            (Node 'A'  
                (Node 'A' Empty Empty)  
                (Node 'C' Empty Empty)))  

-- pattern matching only
changeTop :: Tree Char -> Tree Char
changeTop (Node x l (Node y (Node _ m n) r)) = Node x l (Node y (Node 'P' m n) r)

-- using directions
data Direction = L | R deriving (Show)
type Directions = [Direction]

changeTop' :: Directions -> Tree Char -> Tree Char
changeTop' (L:ds) (Node x l r) = Node x (changeTop' ds l) r 
changeTop' (R:ds) (Node x l r) = Node x l (changeTop' ds r)
changeTop' [] (Node _ l r) = Node 'P' l r 

elemAt :: Directions -> Tree a -> a
elemAt (L:ds) (Node _ l _) = elemAt ds l 
elemAt (R:ds) (Node _ _ r) = elemAt ds r 
elemAt [] (Node x _ _) = x

type Breadcrumbs = [Direction]

goLeft :: (Tree a, Breadcrumbs) -> (Tree a, Breadcrumbs)
goLeft (Node _ l _, bs) = (l, L:bs)

goRight :: (Tree a, Breadcrumbs) -> (Tree a, Breadcrumbs)
goRight (Node _ _ r, bs) = (r, R:bs)

x -: f = f x

data Crumb a = LeftCrumb a (Tree a) | RightCrumb a (Tree a) deriving (Show)
type Crumbs a = [Crumb a]

goLeft' :: (Tree a, Crumbs a) -> (Tree a, Crumbs a)
goLeft' (Node x l r, bs) = (l, LeftCrumb x r:bs)

goRight' :: (Tree a, Crumbs a) -> (Tree a, Crumbs a)
goRight' (Node x l r, bs) = (r, RightCrumb x l:bs)

goUp' :: (Tree a, Crumbs a) -> (Tree a, Crumbs a)
goUp' (t, LeftCrumb x r:bs) = (Node x t r, bs)
goUp' (t, RightCrumb x l:bs) = (Node x l t, bs)

type Zipper a = (Tree a, Crumbs a)

modify :: (a -> a) -> Zipper a -> Zipper a 
modify f (Node x l r, bs) = (Node (f x) l r, bs)
modify f (Empty, bs) = (Empty, bs)

attach :: Tree a -> Zipper a -> Zipper a 
attach t (_, bs) = (t, bs)

topMost :: Zipper a -> Zipper a 
topMost (t, []) = (t, [])
topMost z = topMost (goUp' z)

goLeft'' :: Zipper a -> Maybe (Zipper a)  
goLeft'' (Node x l r, bs) = Just (l, LeftCrumb x r:bs)  
goLeft'' (Empty, _) = Nothing  
  
goRight'' :: Zipper a -> Maybe (Zipper a)  
goRight'' (Node x l r, bs) = Just (r, RightCrumb x l:bs)  
goRight'' (Empty, _) = Nothing  

goUp'' :: Zipper a -> Maybe (Zipper a)  
goUp'' (t, LeftCrumb x r:bs) = Just (Node x t r, bs)  
goUp'' (t, RightCrumb x l:bs) = Just (Node x l t, bs)  
goUp'' (_, []) = Nothing  


-- runghc this.hs
main = do
    print freeTree
    print $ changeTop freeTree
    let newTree = changeTop' [R, L] freeTree
    print $ elemAt [R, L] newTree
    print $ goLeft (goRight (freeTree, []))
    print $ (freeTree, []) -: goRight -: goLeft
    let newFocus = (freeTree, []) -: goLeft' -: goRight' -: modify (\_ -> 'P') 
    print newFocus
    let newFocus2 = newFocus -: goUp' -: modify (\_ -> 'X')
    print newFocus2
    let farLeft = (freeTree, []) -: goLeft' -: goLeft' -: goLeft' -: goLeft' 
    let newFocus = farLeft -: attach (Node '2' Empty Empty)
    print newFocus
    print $ topMost newFocus
    let coolTree = Node 1 Empty (Node 3 Empty Empty)  
    print coolTree
    print $ return (coolTree,[]) >>= goRight''
    print $ return (coolTree,[]) >>= goRight'' >>= goRight''
    print $  return (coolTree,[]) >>= goRight'' >>= goRight'' >>= goRight''  

    
