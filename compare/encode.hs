-- P10 of 99 : run-length encoding of a list
-- "a", "b", "b", "b", "c", "c" -> (1,a), (3,b), (2,c)

import Data.List (group)

encode :: Eq a => [a] -> [(Int, a)]
encode xs = [(length x, head x) | x <- group xs]


main = do
	print $ encode ["a", "b", "b", "b", "c", "c" ]