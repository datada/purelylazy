import System.IO 

greet h = do
	hPutStrLn h "What is your name?"
	name <- hGetLine h 
	hPutStrLn h $ "Hi " ++ name

withTty = withFile "/dev/tty" ReadWriteMode

main = do
    theInput <- readFile "input.txt"
    writeFile "output.txt" (reverse theInput)

    putStrLn "name"
    name <- getLine
    putStrLn ("Hi "++ name)

    putStrLn "name" >> getLine >>= putStrLn . ("Hi "++)

    withTty greet

