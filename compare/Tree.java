
import java.util.List;
import java.util.ArrayList;

public class Tree {

    public static void main(String[] args) {
        System.out.println("ready");
        
        Node[] children  = new Node[] {
        	new Node(new Integer(2), new Node[] {}), 
        	new Node(new Integer(3), new Node[] {}), 
        	new Node(new Integer(4), new Node[] {}), 
        };
        Node root = new Node(new Integer(1), children);
        System.out.println("sum: ");
        System.out.println(root.sum());
    }
}

class Node {

	Node[] children;
	Object myvalue;

	public Node(Object val, Node[] children) {
		myvalue = val;
		this.children = children;
	}

	public int sum() {

		int total = ((Integer)myvalue).intValue();

		if (null == children) {
			System.out.println("no child");
			return total;
		}

		Node child;
		for (int i = 0; i < children.length; i +=1 ){
			child = (Node)children[i];
			total += child.sum();
		}
		return total;
	}
}