-- runhaskell quicksort.hs

quicksort :: (Ord a) => [a] -> [a]
quicksort [] = []
quicksort (x:xs) = 
	quicksort lt ++ [x] ++ quicksort ge
	where
		lt = [y | y <- xs, y < x]
		ge = [y | y <- xs, x <= y]

main = do
	print $ quicksort [2, 3, 9, 5, 2, 8, 7]