-- $ ghci
-- > :load Main.hs
-- $ main

-- $ runghc Main.hs

-- $ ghc --make Main.hs
-- $ ./Main

module Main where

main = do
	putStrLn "quit?"
	ans <- getLine
	if ans /= "y" then do
		putStrLn "continuing"
		main 
	else return ()