-- ch2 LYH
[(a, b, c) | a <- [1..10], b <- [1..10], c <- [1..10], a^2 + b^2 = c^2, a + b + c = 24]

--- in py, [(a,b,c) for c in range(1,11) for b in range(1,c) for a in range(1,b) if (a**2 + b**2 == c**2) and (a+b+c == 24)]


-- ch 3 Types
read "f" -- fails
read "4" :: Int 
read "[1,2,3]" :: [Int]
read "(3, 'a')" :: (Int, Char)

-- Integral
20::Int 
20::Integer

-- Floating
20::Float 
20::Double 

fromIntegral ::(Num b, Integral a) => a -> b 
fromIntegral (length [1,2,3]) + 3.2 -- because lenght :: [a] -> Int not Num

-- ch 4 Syntax

-- pattern matching vs guard vs case
head :: [a] -> a
head (x:_) = x
head [] = error "empty list"

head xs | null xs = eorror "empyt list"
        | otherwise = xs !! 0

head xs = case xs of 
  [] -> error "empty list"
  (x:_) -> x

-- e.g.
data Move = Rock | Paper | Scissors deriving (Eq, Read, Show, Enum, Bounded)
data Outcome = Lose | Tie | Win deriving (Show, Eq, Ord)

outcome :: Move -> Move -> Outcome 
outcome Rock Scissors = Win 
outcome Paper Rock = Win 
outcome Scissors Paper = Win 
outcome us them | us == them = Tie
                | otherwise = Lose 

--> parseMove "Rock \r\n" -> Just Rock
parseMove :: String -> Maybe Move 
perseMove str = case reads str of 
    [(m, rest)] | ok rest -> Just m
    _                     -> Nothin
    where ok = all (`elem` " \r \n")


-- let vs where
slope (x1, y1) (x2, y2) = let dy = y2 - y1
                              dx = x2 - x1
                          in dy/dx

slope (x1, y1) (x2, y2) = dy/dx where 
                                dy = y2 - y1
                                dx = x2 - x1

factorial n0 = let loop acc n | 1 < n = loop (acc * n) (n - 1)
                              | otherwise = acc 
               in loop 1 n0

factoril n0 = loop 1 n0 where
                            loop accn | 1 < n = loop (acc * n) (n -1)

-- nested set of bindings
let y = a * b
    f x = (x+y)/y
in f c + f d 

-- let does not work here!
f x y 
  | z < y = "..."
  | y == z = "..."
  | y < z  = "..."
  where z = x * x 

-- ch 5 Recursion
quicksort  []           =  []
quicksort (x:xs)        =  quicksort [y | y <- xs, y<x ]
                        ++ [x]
                        ++ quicksort [y | y <- xs, y>=x]


-- ch 6 higher order

-- foldr + 0 [1, 2, 3] aka 1 + 2 + 3 + 0
foldr :: (a -> b -> b) -> b -> [a] -> b
foldr f z [] = z
foldr f z (x:xs) = f x (foldr f z xs)

-- foldl + 0 [1, 2, 3] aka 0 + 1 + 2 + 3
foldl : (a -> b -> a) -> a -> [b] -> a 
foldl f z [] = z
foldl f z (x:xs) = foldl f (f z x) xs 

-- use foldl' for real

id = foldr (:) []

reverse = foldl (flip (:)) []

map f = foldr ((:) . f) []

head = foldr (\a bs -> a) undefined
last = foldl (\as b -> b) undefined

filter p xs  = foldr step [] xs where step x ys | p x = x:ys
                                                | otherwise = ys 

foldl  f z xs = foldr step id xs z where step x g a = g (f a x)

-- ch 7 modules

ghci> :m + Data.List Data.Map Data.Set 

import Data.List (nub, sort)

import Data.List hiding (nub)

import qualified Data.Map as M 


-- ch 8 types

data Point = Point Float Float deriving (Show)
data Shape = Circle Point Float | Rectangle Point Point deriving (Show)

surface :: Shape -> Float 
surface (Circle _ r) = pi * r ^ 2 
survace (Rectangle (Point x1 y1) (Point x2 y2)) = (abs $ x2 - x1) * (abs $ y2 - y1)

nudge :: Shapre -> Float -> Float -> Shape 
nudge (Circle (Point x y) r) a b = Circle (Point (x+a) (y+b)) r 
nudge (Rectangle (Point x1 y1) (Point x2 y2)) a b = Rectangle (Point (x1+a) ((y1+b)) (Point (x2+a) (y2+b)))

-- record
data Person = Person { firstName :: String
                     , lastName :: String
                     , age :: Int
                     , height :: Float
                     , phoneNumber :: String
                     , flavor :: String
                     } deriving (Show)

import qualified Data.Map as Map 

data LockerState = Taken | Free deriving (Show, Eq)

type Code = String 

type LockerMap = Map.Map Int (LockerState, Code)

lockerLookup :: Int -> LockerMap -> Either String Code 
lockerLookup lockerNumer map = 
  case Map.lookup lockerNumer map of 
    Nothing -> Left $ "Locker number " ++ show lockerNumer ++ " doesn't exists!"
    Just (state, code) -> if state /= Taken
                            then Right code 
                            else Left $ "Locker " ++ show lockerNumer ++ " is already taken!"


lockers :: LockerMap
lockers = Map.fromList
  [(100, (Taken, "ZD391"))
  ,...]

lockerLookup 101 lockers --> Right "JAH3I"
lockerLookup 100 lockers --> Left "Locker 100 is already taken!"
lockerLookup 102 lockers --> Left "Locker 102 doesn't exist!"


data Tree a = EmptyTree | Node a (Tree a) (Tree a) deriving (Show, Read, Eq)

singleton :: a -> Tree a
singleton x = Node x EmptyTree EmptyTree 

treeInsert :: (Ord a) => Tree a -> Tree a 
treeInsert x EmptyTree = singleton x 
treeInsert x (Node a left rihgt)
  | x == a = Node x left right 
  | x < a  = Node a (treeInsert x left) right 
  | a < x  = Node a left (treeInsert x right)

treeElem :: (Ord a) => a -> Tree a -> Bool 
treeElem x EmptyTree = False 
treeElem x (Node a left right)
  | x == a = True
  | x < a  = treeElem x left 
  | a < x  = treeElem x right 

let nums = [8,6,4,...]
let numsTree = foldr treeInsert EmptyTree nums 
8 `treeElem` numsTree --> True


data TrafficLight = Red | Yellow | Green 

instance Eq TrafficLight where
  Red == Red = True
  Green == Green = True 
  Yellow == Yellow = True 
  _ == _ = False 

instance Show TrafficLight where 
  show Red = "Red light"
  show Yellow = "Yellow light"
  show Green = "Green light"

-- ch 9 IO
main = do 
  putStrLn "name?"
  name <- getLine
  putStrLn ("Hey " ++ name)


main = do
  putStrLn "name?" >> getLine >>= putStrLn . ("Hi "++)


import Control.Monda

main = do
  colors <- forM [1,2,3,4] (\a -> do 
    putStrLn $ "color for number " ++ show a ++ "?"
    color <- getLine
    return color)
  putStrLn "answers:"
  mapM putStrLn colors

-- Main.hs
main = do 
  putStrLn "quit?"
  ans <- getLine
  if ans /= "y" then do 
    putStrLn "more"
    main 
  else return()


-- gchi
-- :load Main.hs
-- main 

-- runghc Main.hs

-- ghc --make Main.hs
-- ./Main


-- capslocker.hs
import Data.Char 

main = do 
  contents <- getContents 
  putStr (map toUpper contents)

$ ghc --make capslocker.hs
$ cat file.txt | ./capslocier


main = do 
  theInput <- readFile "input.txt"
  writeFile "output.txt" (reverse theInput)


main = interact $ unlines . filter ((<10) . length) . lines

-- see todo.hs, args.hs

-- guess the number (random)
-- Byte String
-- Exceptions

-- ch 10 Functional Problem Solving
import Data.List 

solveRPN :: String -> Float 
solveRPN = head . foldl foldingFunction [] . words 
  where foldingFunction (x:y:ys) "*"   = (x * y):ys
        foldingFunction (x:y:ys) "+"   = (x + y):ys
        foldingFunction (x:y:ys) "-"   = (x - y):ys
        foldingFunction (x:y:ys) "/"   = (x / y):ys
        foldingFunction (x:y:ys) "^"   = (x ** y):ys
        foldingFunction (x:xs)   "ln"  = log x:xs
        foldingFunction xs       "sum" = [sum xs]
        foldingFunction xs       n     = read n:xs


ghci> solveRPN "2.7 ln"
ghci> solveRPN "10 10 10 10 sum 4 /"
ghci> solveRPN "10 2 ^"
ghci> solveRPN "43.2425 0.5 ^"


-- ch 11 Functors, Applicative Functors, Monoids
import Data.Char 
import Data.List 

main = do 
  line <- map (intersperse '-' > reverse . map toUpper) getLine
  putStrLn line 

-- (map (lambda (x) (* x 9)) (list 1 2 3))
import Control.Applicative

main = do 
  print $ fmap (\f -> f 9) $ fmap (*) [1,2,3]
  print $ pure (*) <*> [1,2,3] <*> [9]
  pritn $ (*) <$> [1,2,3] <*> [9]


instance Applicative [] where 
  pure x = []
  fs <*> xs = [f x | f <- fs, x <- xs]

[(+), (*)] <*> [1,2] <*> [3,4]

(++) <$> ["ha", "heh"] <*> ["?", "!"]

-- foldable

-- ch 12 & 13 Monads
type Birds = Int 
type Pole = (Birds, Birds)

landLeft :: Birds -> Pole -> Pole 
landLeft n (left, right)
  | abs ((left + n) - right) < 4 = Just (left + n, right)
  | otherwise                    = Nothing 

landRight :: Birds -> Pole -> Pole 
landRight n (left, right)
  | abs (left - (right + n)) < 4 = (left, right + n)
  | otherwise                    = Nothing 

ghci> return (0,0) >>= landRight 2 >>= landLeft 2 >>= landRight 2 --> Just (2,4)

ghci> [1,2] >>= \n -> ['a', 'b'] >>= \ch -> return (n, ch) --> [(1,'a'), (1, 'b'), (2, 'a'), (2, 'b')]

listOfTuples = do 
  n <- [1,2]
  ch <- ['a', 'b']
  return (n, ch)

[(n, ch) | n <- [1,2], ch <- ['a', 'b']]

-- writer
import Control.Monad.Writer 

logNumber :: Int -> Writer [String] Int 
logNumber x = Writer (x, ["Got number: "++  show x])

multWithLog :: Writer [String] Int 
multWithLog = do 
  a <- logNumber 3
  b <- logNumber 5
  return (a * b)

ghci> runWriter multWithLog --> (15, ["Got number: 3", "Got number: 5"])

multWithLog = do 
  a <- logNumber 3
  b <- logNumber 5
  tell ["Goona multiply these two"]
  return (a * b)

ghci> runWriter multWithLog --> (15, ["Got number: 3", "Got number: 5", "Goona multiply these two"])


gcd :: Int -> Int -> Writer [String] Int 
gcd a b
  | b == 0 = do 
    tell ["Finished with "++ show a]
    return a 
  | otherwise = do 
    tell [show a ++ " mod " ++ show b ++ " = " ++ show (a `mod` b)]
    gcd b (a `mod` b)

ghci> fst $ runWriter (gcd 8 3)

ghci> mapM_ putStrLn $ snd $ runWriter (gcd 8 3)

-- better
gcd a b
  | b == 0 = do 
      tell ["Finished with "++ show a]
      return a 
  | otherwise = do 
      result <- gcd b (a `mod` b)
      tell [show a ++ " mod " ++ show b ++ " = " ++ show (a `mod` b)]
      return result

-- with diff list

-- reader

-- state, stack 

import Control.Monad.state 

type Stack = [Int]

pop :: State Stack Int
pop :: state $ \(x:xs) -> (x, xs)

peek :: State Stack Int 
peek = state $ \(x:xs) -> (x, x:xs)

push :: Int -> State Stack ()
push a = state $ \xs -> ((), a:xs)

stackManip :: State Stack Int 
stackManip = do 
  push 3 
  a <- pop 
  pop

ghci> runState stackManip [5,8,2,1] --> (5, [8,2,1])


import System.Random 
import Control.Monad.State 

randomSt :: (RandomGen g, Rnadom a) => State g a 
randomSt = State random 

threeCoins :: State StdGen (Bool, Bool, Bool)
threeCoins = do 
  a <- randomSt 
  b <- randomSt 
  c <- randomSt
  return (a, b, c)

ghci> runState threeCoins (mkStdGen 33) --> ((True, False, True), 680029187 2103410263)


m1 >>= \x1 ->
m2 >>= \x2 ->
...
mn >>= \xn -> f x1 x2 ... xn

do
  x1 <- m1
  x2 <- m2 
  ...
  xn <- mn 
  f x1 x2 ... xn

-- monadic functions

-- making monads

-- ch 14 zippers




