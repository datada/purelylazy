
-- ghc -o args args.hs
-- ./args one 2 "three 3"

import System.Environment

main = do
	progName <- getProgName
	putStrLn "The program name is:"
	putStrLn progName 
	args <- getArgs
	putStrLn "The arguments are:"
	putStrLn $ show args
	