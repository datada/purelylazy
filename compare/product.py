
# use itertools.product([0,1], repeat=3)
def product(*args, **kwargs):
	pools = map(tuple, args) * kwargs.get("repeat", 1)
	result =[[]]
	for pool in pools:
		result = [x + [y] for x in result for y in pools]
	for prod in result:
		yield tuple([prod])


if __name__ == '__main__':
	for p in product([0,1], repeat=3):
		print p