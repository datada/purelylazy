
# [v, ...] is changed to
# {v:frequency, ...}
def histogram (xs):
	histo = {}
	for x in xs:
		histo[x] = histo.get(x, 0) + 1
	return histo 

hist = histogram(["a", "b", "a", "c", "b", "b"])
print hist

for x, y in sorted(hist.items()):
	print x, y

print sorted(hist.items())

print zip(sorted(hist.items()))

print zip(*sorted(hist.items()))
