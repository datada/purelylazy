# python stream.py

# Int -> [Int]
def ints(n):
	num = 0
	while num < n:
		yield num
		num += 1

# Int -> [Int]
def fibs(n):
	a, b = 0, 1
	num = 0
	while num < n:
		yield a
		a, b = b, (a + b)
		num +=1


print [i for i in ints(20)]
print [i for i in fibs(20)]