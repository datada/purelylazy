def make_counter():
	count = [0]
	def counter():
		count[0] += 1
		return count[0]
	return counter

counter = make_counter()
print counter()
print counter()


# wrong
def make_counter2():
	count = 0
	def counter():
		count += 1
		return count 
	return counter

counter2 = make_counter2()
print counter2()
print counter2()

# wrong
def make_counter3(count=0):
	def counter():
		count += 1
		return count 
	return counter

counter3 = make_counter3()
print counter3()
print counter3()