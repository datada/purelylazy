import System.Environment (getArgs)
import System.FilePath (normalise, joinPath, takeFileName)

topPath = "/Users/user/some/path"

amendPath relPath = "ln -s " ++ (normalise . joinPath) [topPath, relPath] ++ " " ++ takeFileName relPath

main = mainWith myFunction
  where mainWith f = do
          args <- getArgs
          mapM putStrLn $ (f args)

        myFunction relativePaths = map amendPath relativePaths