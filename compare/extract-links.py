
# from Udacity CS101 Unit 2
page = ""

with open("some.html", "r") as f:
	page = "".join(f.readlines())

def get_next_target(s):
	start_link = s.find("<a href=")
	if start_link < 0:
		return (None, 0)

	start_quote = s.find('"', start_link)
	end_quote = s.find('"', start_quote + 1)

	url = s[start_quote+1:end_quote]

	return (url, end_quote)

def print_all_links(page):
	while True:
		(url, endpos) = get_next_target(page)
		if url:
			print url
			page = page[endpos:]
		else:
			break

print_all_links(page)

