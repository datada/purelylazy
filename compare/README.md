### IO

repl*

args* (get arguments from commandline)
echo.* (IO)
hello.* (Interactive IO)

interact.hs e.g. of (String -> String) -> IO ()

sleep.*

Main.hs
name.hs

### Scope

closure.py (late binding error)
closure.rkt
scope.py
namespace.rkt

### List Comp

triangle.*
quicksort.* 

### list & dict

histo.* 
encode.*
slicing.py

### Lazy

stream.*
lazy.*

### Tree

tree.*
bf* (breadth first)
find-files.*

### pattern matching
vector-add.*

### Control

unzip.rkt (continuation passing)
gen.py
generator.rkt
state*


### Toy apps

rpn.* 
todo.*
trie*


### basic Haskell

Main.hs
name.hs
note.hs
applicative.hs
state*.hs
quicksort.hs 
prime.hs
todo.hs
rpn.hs
zippers.hs

