
bfnum' n [] = []

bfnum' n [E]
    = (E, [])
    = enq (bfnum' n [], E)
    = enq ([], E)
    = [E]

bfnum' n [E, E]
    = (E, [E])
    = enq (bfnum' n [E], E)
    = enq ([E], E)
    = [E, E]

bfnum' 1 [T(x,E,E)] = (T(x,E,E), [])
    q           = enq (enq ([], E), E) = [E, E]
    q'          = bfnum' 2 [E, E] = [E, E]
    (b', q'')   = deq q'  = deq [E, E] = (E, [E])
    (a', ts')   = deq q'' = deq [E] = (E, [])
    = enq ([], T(1, a', b')) = [T(1, E, E)]

bfnum' 2 [E, E, E, E] = (E, [E, E, E]) 
    = enq (bfnum' 2 [E, E, E], E)
        = enq (bfnum' 2 [E, E], E)
        ...
    = [E, E, E, E]

bfnum' 1 [T(x,E,E), E , E] = (T(x,E,E), [E, E])
    q           = enq (enq ([E, E], E), E) = [E, E, E, E]
    q'          = bfnum' 2 [E, E, E, E] = [E, E, E, E]
    (b', q'')   = deq q'  = deq [E, E, E, E] = (E, [E, E, E])
    (a', ts')   = (E, [E, E])
    = enq ([E, E], T(1, E, E)) = [E, E, T(1, E, E)]

bfnum' 2 [T(y, E, E), T(z, E, E)] = (T(y, E, E), [T(z, E, E)])
    q           = enq (enq ([T(z, E, E)], E), E) = [T(z, E, E) E, E]
    q'          = bfnum' (2+1) [T(z, E, E) E, E] = [E, E, T(3, E, E)]
    (b', q'')   = (E, [E, T(3, E, E)])
    (a', ts')   = (E, [T(3, E, E)])
    = enq (ts', T(2, E, E))
    = [T(3, E, E), T(2, E, E)]

bfnum' 1 [T(x, T(y, E, E), T(z, E, E))] = (T(x, T(y, E, E), T(z, E, E)), [])
    q           = enq (enq ([], T(y, E, E)), T(z, E, E)) = [T(y, E, E), T(z, E, E)]
    q'          = bfnum' (1+1) [T(y, E, E), T(z, E, E)] = [T(3, E, E), T(2, E, E)]
    (b', q'')   = (T(3, E, E), [T(2, E, E)])
    (a', ts')   = (T(2, E, E), [])
    = enq ([], T(1, a', b'))
    = [T(1, T(2, E, E), T(3, E, E))]

    