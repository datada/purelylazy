# P7
# what do you know, flatten is not a built in!
b = [[1,2,3],[4,5,6]]

# Danny Yoo
def flatten(iterable):
	it = iter(iterable)
	for e in it:
		if isinstance(e, (list, tuple)):
			for f in flatten(e):
				yield f
		else:
			yield e

print [i for i in flatten(b)]

# P8 of 99 remove dups

def remove_dups(xs):
    return [x for i,x in enumerate(xs) if i == 0 or x != xs[i-1]]

print remove_dups(["a", "b", "b", "b", "c", "c"])

# P10 of 99 : run-length encoding of a list
# "a", "b", "b", "b", "c", "c" -> (1,a), (3,b), (2,c)

def encode(xs):
    from itertools import groupby
    return [[len(list(group)), key] for key, group in groupby(xs)]

print encode(["a", "b", "b", "b", "c", "c"])

