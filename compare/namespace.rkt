#lang racket

(define-namespace-anchor top)

(parameterize ((current-namespace (namespace-anchor->namespace top)))
	(eval '(define x 10))
	(namespace-variable-value 'x))

