-- from rosetta code

import qualified Data.Map as M

initial = M.fromList $ zipWith (\k v -> (toEnum k, v)) [0..255] (repeat 0) 

main = do
    text <- readFile "histo3.hs"
    let result = foldl (flip (M.adjust (+1))) initial text
    mapM_ print $ M.toList result
    