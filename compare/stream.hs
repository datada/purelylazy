
windowsOf :: Int -> [a] -> [[a]]
windowsOf n xs 
	| length xs < n = [[]]
	| otherwise = (take n xs):(windowsOf n (tail xs))


-- runhaskell stream.hs
main = do
	print $ take 10 (windowsOf 3 [1..1000])
	print $ [10000, 10001..1000000000] !! 45006230
