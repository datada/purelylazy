var zipper = function (obj, crumbs) {
	return {
		obj: function () {
			return obj;
		},
		crumbs: function () {
			return crumbs;
		},
	  state: function () {
	    return {"obj": obj, "crumbs": crumbs};
	  },
	  goDown: function (key) {
	    if (key in obj) {
	      // order is critical!
	      var rest = obj;// let's not worry about putting a hole, going back up will fill it up, maybe
	      //_.omit(obj, key);// converts array to obj, 
	      console.log("saving", rest);
	      crumbs.push({"key":key, "rest":rest});
	      obj = obj[key];
	      return this;
	    }
	    console.log("cannot go down to", key, "in", obj);
	    return this;
	  },
	  goUp: function () {
	    if (_.isEmpty(crumbs)) {
	      console.log("already at the top");
	      return this;
	    }
	    var crumb = crumbs.pop();
	    var key = crumb["key"];
	    var rest = crumb["rest"];
	    rest[key] = obj;
	    obj = rest;

	    return this;
	  },
	  topMost: function () {
	    if (_.isEmpty(crumbs)) {
	      console.log("at the top");
	      return this;
	    }
	    return this.goUp().topMost();
	  },
	  modify: function (proc) {
	  	obj = proc(obj);
	  	return this;
	  },
	  attach: function (stuff) {
	  	obj = stuff;
	  	return this;
	  }
	};
};