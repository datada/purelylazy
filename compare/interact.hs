
-- usage:
-- runghc this.hs < this.hs
main = interact wordCount
	where wordCount input = show (length (lines input)) ++ "\n"