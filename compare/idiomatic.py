#Pycon 2013

"""
Transforming Code into Beautiful, Idiomatic Python
Raymond Hettinger 
Pycon 2013
"""

for i in [0, 1, 2, 3, 4, 5]:
	print i ** 2

for i in range(6):
	print i**2

colors = ['red', 'green', 'blue', 'yellow']

# bad
for i in range(len(colors)):
	print colors[i]

# badass
for color in colors:
	print color

# bad
for i in range(len(colors)-1, -1, -1):
	print colors[i]

# badass
for color in reversed(colors):
	print color 

# bad
for i in range(len(colors)):
	print i, '-->', colors[i]

# badass
for i, color in enumerate(colors):
	print i, '-->', color 

names = ['ray', 'rachel', 'matt']

# bad
n = min(len(names), len(colors))
for i in range(n):
	print names[i], '-->', colors[i]

# badass
for name, color in zip(names, colors):
	print name, '-->', color

# best
from itertools import izip
for name, color in izip(names, colors):
	print name, '-->', color

for color in sorted(colors):
	print color 

for color in sorted(colors, reverse=True):
	print color 

def compare_length(c1, c2):
	if len(c1) < len(c2): return -1
	if len(c1) < len(c2): return 1
	return 0

# bad
print sorted(colors, cmp=compare_length)

# badass
print sorted(colors, key=len)

# until Sentinel value
# with open('idiomatic.py', 'r') as f:
# 	blocks = []
# 	while True:
# 		block = f.read(32)
# 		if blocks == '':
# 			break 
# 		blocks.append(block)

# with open('idiomatic.py', 'r') as f:
# 	blocks = []
# 	for block in iter(partial(f.read, 32), ''):
# 		blocks.append(block)


# multiple exit in loops
def find(haystack, needle):
	found = False
	for pos, hay in enumerate(haystack):
		if hay == needle:
			found = True
			break 
	if not found:
		return -1
	return i


# for else
def find(haystack, needle):
	for pos, hay in enumerate(haystack):
		if hay == needle:
			break;
	else:
		return -1 # for loop completed without breaking
	return pos


d = {
'matt': 'blue',
'rachel': 'green',
'raymond': 'red'
}

for k in d:
	print k 

for k in d.keys():
	if k.startswith('r'):
		del d[k]

#  not worky worky
d2 = {
'matt': 'blue',
'rachel': 'green',
'raymond': 'red'
}
# d3 = {k: d2[k] for k in d2 if not k.startswith['r']}


for k in d2:
	print k, '-->', d2[k]

for k, v in d2.items():
	print k, '-->', v 

for k, v in d2.iteritems():
	print k, '-->', v 

print dict(izip(names, colors))
print dict(enumerate(names))


# histogram
colors = ['red', 'green', 'red', 'blue', 'green', 'red']

d = {}
for color in colors:
	if color not in d:
		d[color] = 0
	d[color] += 1
print d 

d = {}
for color in colors:
	d[color] = d.get(color, 0) + 1
print d

from collections import defaultdict 

d = defaultdict(int)
for color in colors:
	d[color] += 1

# grouping
names = ['raymond', 'rachel', 'matthew', 'roger', 'betty', 'melissa', 'judith', 'charlie']

d = {}
for name in names:
	key = len(name)
	if key not in d:
		d[key] = []
	d[key].append(name)
print d

d = {}
for name in names:
	key = len(name)
	d.setdefault(key, []).append(name)
print d

d = defaultdict(list)
for name in names:
	key = len(name)
	d[key].append(name)
print d

d = {
'matt': 'blue',
'rachel': 'green',
'raymond': 'red'
}
while d:
	key, value = d.popitem()
	print key, '-->', value

import argparse, os
# linking dictionaries
defaults = {'color': 'red', 'user': 'guest'}
parser = argparse.ArgumentParser()
parser.add_argument('-u', '--user')
parser.add_argument('-c', '--color')
namespace = parser.parse_args([])
command_line_args = {k:v for k,v in vars(namespace).items() if v}

#  bad
d = defaults.copy()
d.update(os.environ)
d.update(command_line_args)

# badass 3.3
# from collections import ChainMap
# d = ChainMap(command_line_args, os.environs, defaults)

# keyword args

# bad
# twitter_search('@obama', False, 20, True)

# badass
# twitter_search('@obama', retweets=False, numtweets=20, popular=True)

# named tuples

# bad
# doctest.testmod() ->(0, 4)

# badass
# doctest.testmod() -> TestResults(failed=0, attempted=4)

#  TestResults = namedtuple('TestResults', ['failed', 'attempted'])

from collections import namedtuple
Point = namedtuple('Point', 'x y')
pt1 = Point(1.0, 5.0)
pt2 = Point(2.5, 1.5)

from math import sqrt
line_length = sqrt((pt1.x-pt2.x)**2 + (pt1.y-pt2.y)**2)


# unpacking
p = 'Raymond', 'Hettinger', 0x30, 'Python@example.com'

# bad
fname = p[0]
lname = p[1]
age = p[2]
email = p[3]

fname, lname, age, email = p

# multiple state vars

# bad
def fibonacci(n):
	x = 0
	y = 1
	for i in range(n):
		print x 
		t = y 
		y = x + y 
		x = t 

# badass
def fibonacci(n):
	x, y = 0, 1
	for i in range(n):
		print x 
		x, y = y, x+y 

# simultaneous state updates

# bad
tmp_x = x + dx * t 
tmp_y = y + dy * t 
tmp_dx = influence(m, x, y, dx, dy, partial='x')
tmp_dy = influence(m, x, y, dx, dy, partial='y')
x = tmp_x 
y = tmp_y
dx = tmp_dx 
dy = tmp_dy 

# badass
x, y, dx, dy = (x + dx * t, 
				y + dy * t, 
				influence(m, x, y, dx, dy, partial='x'),
				influence(m, x, y, dx, dy, partial='y'))

names = ['raymond', 'rachel', 'matthew', 'roger', 'betty', 'melissa', 'judith', 'charlie']

# bad
s = names[0]
for name in names[1:]:
	s += ', ' + name 
print s 

print ', '.join(names)

# bad
del names[0]
names.pop(0)
names.insert(0, 'mark')

# badass
names = deque(['raymond', 'rachel', 'matthew', 'roger', 'betty', 'melissa', 'judith', 'charlie'])


del names[0]
names.popleft()
names.appendleft('mark')


# decorators

# bad
def web_lookup(url, saved={}):
	if url in saved:
		return saved[url]
	page = urllib.urlopen(url).read()
	saved[url] = page
	return page 

# badass
@cache
def web_lookup(url):
	return urllib.urlopen(url).read()

def cache(func):
	saved = {}
	@wraps(func)
	def newfunc(*args):
		if args in saved:
			return newfunc(*args)
		result = func(*args)
		saved[args] = result 
		return result 
	return newfunc

# factor-out temporary contexts

# bad
old_context = getcontext().copy()
getcontext().prec = 50
print Decimal(355) / Decimal(113)
setcontext(old_context)

# badass
with localcontext(Context(prec=50)):
	print Decimal(355) / Decimal(113)

f = open('data.txt')
try: 
	data = f.read()
finally:
	f.close()

with open('data.txt') as f:
	data = f.read()


lock = threading.Lock()

# bad
lock.acquire()
try:
	print "critical section 1" 
	print "critical section 2"
finally:
	lock.release()

# badass
with lock:
	print "critical section 1"
	print "critical section 2"

# bad
try:
	os.remove('somefile.tmp')
except OSError:
	pass 

# badass
with ignored(OSError):
	os.remove('somefile.tmp')

@contextmanager
def ignored(*exceptions):
	try:
		yield
	except exceptions:
		pass 

# bad
with open('help.txt', 'w') as f:
	oldstdout = sys.stdout
	sys.stdout = f
	try:
		help(pow)
	finally:
		sys.stdout = oldstdout

# badass
with open('help.txt', 'w') as f:
	with redirect_stdout(f):
		help(pow)

@contextmanager
def redirect_stdout(fileobj):
	oldstdout = sys.stdout
	sys.stdout = fileobj
	try:
		yield fileobj
	finally:
		sys.stdout = oldstdout

# list comp and generators

result = []
for i in range(10):
	s = i ** 2
	result.append(s)
print sum(result)

print sum([i**2 for i in xrange(10)])

print sum(i**2 for i in xrange(10))















