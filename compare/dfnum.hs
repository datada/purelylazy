import Data.Char
import Control.Monad.State

data Tree a = Leaf a | Branch (Tree a) (Tree a)
                deriving (Eq, Show)

instance Functor Tree where
  fmap f (Leaf a) = Leaf (f a)
  fmap f (Branch l r) = Branch (fmap f l) (fmap f r)              


zipTree :: Tree a -> Tree b -> Tree (a, b)
zipTree (Leaf a) (Leaf b) = Leaf (a, b)
zipTree (Branch l r) (Branch l' r') = Branch (zipTree l l') (zipTree r r')


safeZipTree :: Tree a -> Tree b -> Maybe (Tree (a, b))
safeZipTree (Leaf a) (Leaf b) = return (Leaf (a, b))
safeZipTree (Branch l r) (Branch l' r') = safeZipTree l l' >>= \l'' ->
                                          safeZipTree r r' >>= \r'' ->
                                          return (Branch l'' r'')
safeZipTree _ _ = Nothing 


dfNum' :: Tree a -> Int -> (Tree Int, Int)
dfNum' (Leaf a) n = (Leaf n, n + 1)
dfNum' (Branch l r) n = let (l', n') = dfNum' l n
                            (r', n'') = dfNum' r n'
                        in (Branch l' r', n'')

dfNum :: Tree a -> Tree Int
dfNum t = fst $ dfNum' t 1


type Cnt = Int 
tick :: State Cnt Int 
tick = state $ \n -> (n, n+1)

tickTock :: State Cnt Int
tickTock = do
    a <- tick
    b <- tick
    return b

mlabel :: Tree a -> State Cnt (Tree Int) 
mlabel (Leaf a) = do
    n <- tick
    return (Leaf n)
mlabel (Branch l r) = do
    l' <- mlabel l 
    r' <- mlabel r 
    return (Branch l' r')

label :: Tree a -> Tree Int 
label t = fst $ runState (mlabel t) 1


main = do
    let t = Branch (Leaf "a") (Branch (Leaf "b") (Leaf "c"))
    print t 
    print $ fmap (\cs -> map toUpper cs) t
    print $ zipTree t (fmap (\cs -> map toUpper cs) t)
    print $ safeZipTree t (fmap (\cs -> map toUpper cs) t)
    print $ dfNum t
    print $ runState tick 1
    print $ runState tickTock 1
    print $ label t