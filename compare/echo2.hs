-- from http://book.realworldhaskell.org/read/functional-programming.html

-- ghc --make echo2.hs
-- ./echo2 echo2.hs tmp.txt
-- cat tmp.txt

import System.Environment (getArgs)

interactWith function inputFile outputFile = do
    input <- readFile inputFile
    writeFile outputFile (function input)

main = mainWith myFunction 
    where 
        mainWith function = do
            args <- getArgs
            case args of 
                [input, output] -> interactWith function input output 
                _ -> putStrLn "error: exactly 2 args neeeded"
        myFunction = id 

        
          
