### Effective Django + DjangoProject tutorial

#### Quick Start

```sh
mkdir djproj
virtualenv djproj

cd djproj
source bin/activate
echo "Django=1.5.1" > requirements.txt
pip install -U -r requirements.txt

python -c "import django; print(django.get_version())"

django-admin.py startproject mysite
cd mysite

curl https://raw.github.com/github/gitignore/master/Python.gitignore > .gitignore
echo "*.sql" >> .gitignore
git init 
git add .
git commit -m 'initial django installation'

git checkout -b models

open http://localhost:8080
python manage.py runserver 8080
ctrl-c

```

```py
DATABASES = {
	'defaults': {
		'ENGINE': 'django.db.backends.sqlite3',
		'NAME': 'mysite.sql3',
	}
}
```

```sh
python manage.py syncdb

python manage.py startapp polls
```

```py
import datetime
from djago.utils import timezone
from django.db import models


class Poll(models.Model):
	question = models.CharField(max_length=200)
	pup_date = models.DateTimeField('date published')

	def __unicode__(self):
		return self.question

	def was_published_recently(self):
		yesterday = timezone.now() - datetime.timedelta(days=1)
		return yesterday < self.pub_date 


class Choice(models.Model):
	poll = models.ForeignKey(Poll)
	choice_next = models.CharField(max_length=200)
	votes = models.IntegerField(default=0)

	def __unicode__(self):
		return self.choice_text

INSTALLED_APPS = {
	'polls',
}
```

```sh
python manage.py sql polls
python manage.py syncdb
python manage.py validate
python manage.py shell

# to redo
python manage.py sqlclear polls

git add .
git status
git commit -m 'models implemented'
git checkout master
git merge models
```