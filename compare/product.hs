prod :: Show a => [a] -> Int -> [[a]]
prod = prod' [[]]

prod' :: Show a => [[a]] -> [a] -> Int -> [[a]]
prod' accum _ 0 = accum
prod' accum as n = prod' result as (n - 1) where
    result = [ x ++ [a] |  x <- accum, a <- as]


main = do
    print $ prod [0, 1] 2
    print $ prod [0, 1] 3
