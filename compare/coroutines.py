# from http://www-inst.eecs.berkeley.edu/~cs61a/sp12/book/streams.html
# python3

def match(pattern):
	print("Looking for "+pattern)

	try:
		while True:
			s = (yield)
			if pattern is s:
				print(s)
			else:
				print(".")
	except GeneratorExit:
		print("===Done====")

def test():
	m = match("Jabberwock")
	m.__next__()
	m.send("the Jabberwock with eyes of flame")
	m.send("came whiffling through the tulgey wood")
	m.send("and burbled as it came")
	m.close()

# "Jabberwock" in "the Jabberwock with eyes of flame" -> True but
# match fails to print "the Jabberwock with eyes of flame"
# WTF?