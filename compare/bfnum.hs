import Control.Monad.State

--  breadth first numbering
--  based on http://www.eecs.usma.edu/webs/people/okasaki/icfp00bfn.pdf
--  http://learnyouahaskell.com/making-our-own-types-and-typeclasses

data Tree a = EmptyTree | Node a (Tree a) (Tree a) deriving (Show, Read, Eq)

singleton :: a -> Tree a
singleton x = Node x EmptyTree EmptyTree

treeInsert :: (Ord a) => a -> Tree a -> Tree a
treeInsert x EmptyTree = singleton x
treeInsert x (Node a left right)
    | x == a = Node x left right
    | x < a  = Node a (treeInsert x left) right
    | x > a  = Node a left (treeInsert x right)

treeElem :: (Ord a) => a -> Tree a -> Bool
treeElem x EmptyTree = False
treeElem x (Node a left right)
    | x == a = True
    | x < a = treeElem x left
    | x > a = treeElem x right

nums = [8,6,4,1,7,3,5]
numsTree = foldr treeInsert EmptyTree nums 


enq :: [a] -> a -> [a]
enq q a = q ++ [a]

deq :: [a] -> (a, [a])
deq [] = error "No deq for empty list"
deq (x:xs) = (x, xs)


bftrav' :: [Tree a] -> [a]
bftrav' [] = []
bftrav' q = case deq q of
    (EmptyTree, ts) -> bftrav' ts
    (Node x left right, ts) -> x:(bftrav' $ (enq (enq ts left) right))

-- walk the tree
bftrav :: Tree a -> [a]
bftrav t = bftrav' (enq [] t)

bfnum' :: [Tree a] -> Int -> [Tree Int]
bfnum' [] _ = []
bfnum' q i = case deq q of
    (EmptyTree, ts) -> enq (bfnum' ts i) EmptyTree
    (Node x left right, ts) ->  let 
                                    q1       = enq (enq ts left) right
                                    q2       = bfnum' q1 (i + 1)
                                    (y, q3) = deq q2
                                    (z, ts')  = deq q3
                                in enq ts' (Node i z y) 

bfnum :: Tree a -> Tree Int
bfnum t = let
            q = enq [] t 
            q' = bfnum' q 1
            (t', _) = deq q'
          in t'

type Cnt = Int
tick :: State Cnt Int
tick = state $ \n -> (n, n+1)

-- not worky worky ... state of Cnt is not what I think it is...S
mbflabel :: [Tree a] -> State Cnt ([Tree Int])
mbflabel [] = return []
mbflabel q = case deq q of
    (EmptyTree, ts)         -> mbflabel ts >>= \ts' -> return (enq ts' EmptyTree)
    (Node x left right, ts) -> let q1 = enq (enq ts left) right
                               in mbflabel q1 >>= \q2 -> let (y, q3) = deq q2
                                                             (z, ts') = deq q3
                                                         in tick >>= \n -> return (enq ts' (Node n z y)) 

mbfnum :: Tree a -> Tree Int
mbfnum t = let q = enq [] t 
               q' = fst (runState (mbflabel q) 1)
               (t', _) = deq q'
            in t'

main = do
    print $ numsTree
    print $ 8 `treeElem` numsTree
    print $ 100 `treeElem` numsTree
    print $ bftrav $ Node 'a' (Node 'b' EmptyTree (singleton 'c')) (singleton 'd')
    print $ bfnum $ Node 'a' (Node 'b' EmptyTree (singleton 'c')) (singleton 'd')
    let t = Node 'a' (Node 'b' EmptyTree (singleton 'c')) (singleton 'd')
    print $ mbfnum t