-- runhgc lazy.hs

largestDivisible :: (Integral a) => a
largestDivisible = head (filter p [100000,99999..])
	where p x = x `mod` 3829 == 0

main = do
	print $ largestDivisible