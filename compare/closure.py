# wrong
def create_multipliers():
	return [lambda x : x * i for i in range(5)]

# wrong fix...lambda is not the culprit
def create_multipliers2():
	multipliers = []

	for i in range(5):
		def multiplier(x):
			return x * i 
		multipliers.append(multiplier)

	return multipliers

def create_multipliers3():
	return [lambda x, i=i : x * i for i in range(5)]

if __name__ == '__main__':
	for multiplier in create_multipliers():
		print multiplier(2)
		print "----" * 5
	for multiplier in create_multipliers2():
		print multiplier(2)		
		print "++++" * 5
	for multiplier in create_multipliers3():
		print multiplier(2)	
		print "****" * 5