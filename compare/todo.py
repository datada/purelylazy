
# python todo.py view todo.txt
# python todo.py add todo.txt -d "make souffle"
# python todo.py add todo.txt -d "make creme burlee"
# python todo.py add todo.txt -d "clean dishes
# python todo.py view todo.txt
# python todo.py remove todo.txt -n 2
# python todo.py view todo.txt

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("action", help="what to do")
parser.add_argument("filename", help="name of file that records items")
parser.add_argument("-d", "--detail", help="description of item")
parser.add_argument("-n", "--number", help="number of item to remove", type=int)
args = parser.parse_args()

if args.action == "add":
    with open(args.filename, 'a') as f:
        f.write(args.detail)
        f.write("\n")

if args.action == "view":
    with open(args.filename) as f:
        for i, line in enumerate(f):
            print "{0} - {1}".format(i, line.strip())

if args.action == "remove":
    newitems = []
    with open(args.filename) as f:
        for i, line in enumerate(f):
            if i != args.number:
                newitems.append(line.strip())
    with open(args.filename, 'w') as f:
        for item in newitems:
            f.write(item)
            f.write("\n")