

data Tree a = Tree a [Tree a]
                deriving (Show)

instance Functor Tree where
  fmap f (Tree a ts) = Tree (f a) (map (fmap f) ts)              

main = do
    print $ Tree 1 []
    print $ fmap (*2) $ Tree 1 []
    print $ Tree 5 [Tree 3 [Tree 1 [], Tree 4[]], Tree 7 []]
    print $ fmap (*2) $ Tree 5 [Tree 3 [Tree 1 [], Tree 4[]], Tree 7 []]
