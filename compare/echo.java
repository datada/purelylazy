import java.io.*;

class Echo {
	public static void main(String[] args) throws Exception {
		BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
		String s;
		// awkward place to exit
		do {
			s = keyboard.readLine();
			// we want to exit here and skip processing empty string
			process(s);
		} while (s.length() > 0);
	}

	public static void process(String s) {
		System.out.println(s);
	}
}

class Echo2 {
	public static void main(String[] args) throws Exception {
		BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
		String s = keyboard.readLine();// repeat
		while (s.length() > 0) {
			process(s);
			s = keyboard.readLine(); //repeat
		}
	}

	public static void process(String s) {
		System.out.println(s);
	}
}

class Echo3 {
	public static void main(String[] args) throws Exception {
		BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
		String s;
		while (true) {
			s = keyboard.readLine();
			if (s.length() == 0) {
				break; // not a natural end point
			}
			process(s);
		}
	}

	public static void process(String s) {
		System.out.println(s);
	}
}