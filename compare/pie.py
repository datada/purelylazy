import random


# Int -> [(U, U)] where Y is between 0 and 1
def unit_square(n):
	for i in range(n):
		yield (random.random(), random.random())


# Int -> Number
def pie(n):
	return 4.0 * len([(x, y) for (x,y) in unit_square(n) if (x*x + y*y) < 1]) / n


if __name__ == '__main__':
	print pie(300000)