#lang racket

;; not using built in (require racket/promise) or lazy racket
;; from http://pl.barzilay.org/lec20.txt
(define-syntax-rule (cons-stream x y) 
  (cons x (lambda () y)))

(define stream? pair?)

(define the-empty-stream null)
(define stream-null? null?)

(define stream-car car)
(define (stream-cdr s) 
  ((cdr s)))


(define (stream-filter pred stream)
  (cond ((stream-null? stream) the-empty-stream)
        ((pred (stream-car stream))
         (cons-stream (stream-car stream)
                      (stream-filter pred
                                     (stream-cdr stream))))
        (else (stream-filter pred (stream-cdr stream)))))

;(define  (add-streams s1  s2)  (stream-map +  s1  s2))


(define (divisible? x y) (= (remainder x y) 0))

(define (count-down n)
  (cons-stream n (count-down (sub1 n))))

(define (largest-divisible)
  (stream-car (stream-filter (lambda (n)
                               (divisible? n 3829))
                             (count-down 100000))))

(largest-divisible)


