# original code at http://www.math.bas.bg/bantchev/place/rpn/rpn.python.html

# python rpn.py
# 10 10 *
# 25 5 /

# add more operations ...

import string, operator
ops = {'+': operator.add, '-': operator.sub, '*': operator.mul, '/': operator.div}

while True:
    try:
        st = []
        for tk in string.split(raw_input()):
            if tk in ops:
                y,x = st.pop(), st.pop()
                z = ops[tk](x,y)
            else:
                z = float(tk)
            st.append(z)
        assert len(st) <= 1
        if len(st) == 1: print(st.pop())
    except EOFError: 
        break

