-- from rosetta code
import Data.List

main = do
    readFile "histo2.hs" >>= mapM_ (\x -> print (head x, length x)) . group . sort