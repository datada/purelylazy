
import Data.List
import qualified Data.Map as Map


-- ["a", "b", "a", "c", "b", "b"] is sorted
-- ["a", "a", "b", "b", "b", "c"] is group'd
-- [["a", "a"] ["b", "b", "b"] ["c"]] then changed to
-- [("a", 1), ("b", 3), ("c", 1)] then to "dict"
histogram :: Ord a => [a] -> Map.Map a Int
histogram xs = Map.fromList [(head l, length l) | l <- group (sort xs)]

main = do
    print $ histogram ["a", "b", "a", "c", "b", "b"]