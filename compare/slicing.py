import string
xs = list(string.lowercase)
# xs = xs[:i] + xs[i:j] + xs[j:]
assert(xs ==  xs[:9] + xs [9:15] + xs[15:])

# all but last + last
assert(xs == xs[:-1] + [xs[-1]])

print "reverse"
print xs[::-1]

print "last 10"
print xs[-10::]