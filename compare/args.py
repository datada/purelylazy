# python args.py 1 2 --opts 2 --opts 3 --verbose

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("first", help="first arg")
parser.add_argument("second", help="first arg")
parser.add_argument('--opts', help='optional multiple', action='append', default=[])
parser.add_argument('--verbose', help='presence means true', action='store_true')

args = parser.parse_args()

print args.first
print args.second
print args.opts
print args.verbose


