import sys

# cat some.txt | python echo.py

for line in sys.stdin:
	# \n is left intact
	print line.rstrip()