all_data =[
['Tom', 'Billy', 'Jefferson', 'Andre', 'Wesley', 'Steven', 'Joe'],
['Susie', 'Casey', 'Jill', 'Ana', 'Eva', 'Jennifer', 'Stephanie']
]

print all_data

names_of_interest = []
for names in all_data:
	enough_es = [name for name in names if name.count('e') >= 2]
	names_of_interest.extend( enough_es )

print names_of_interest


print [name for names in all_data for name in names if name.count('e') >= 2]


some_tuples = [(1,2,3), (4,5,6), (7,8,9)]
flattened = [x for tup in some_tuples for x in tup]
print flattened

flattened = []
for tup in some_tuples:
	for x in tup:
		flattened.append(x)
print flattened

print [[x for x in tup] for tup in some_tuples]
