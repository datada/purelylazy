-- from learnyouhaskell
-- ghci
-- solve "2.7 ln"
-- solve "10 10 10 10 sum 4 /"
-- solve "10 2 ^"
-- :quit


import Data.List

solve :: String -> Float
solve = head .foldl f [] . words
    where   f (x:y:ys) "*" = (x * y):ys
            f (x:y:ys) "+" = (x + y):ys
            f (x:y:ys) "-" = (y - x):ys
            f (x:y:ys) "/" = (y / x):ys
            f (x:y:ys) "^" = (y ** x):ys
            f (x:xs) "ln"  = log x:xs
            f xs "sum"     = [sum xs]
            f xs numString = read numString:xs