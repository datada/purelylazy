import os

def find_files(top, extentions):
	for root, dirs, files in os.walk(top):
		for fname in files:
			full_name = os.path.join(root, fname)
			filename, ext = os.path.splitext(fname)
			if extentions:
				if ext[1:] in extentions:
					yield full_name
			else:
				yield full_name


# find local jquery usage in pixbyfunc
# python find_files.py ~/gae/pixbyfunc --ext html --ext htm
if __name__ == '__main__':

	import argparse

	parser = argparse.ArgumentParser()
	parser.add_argument("top", help="top directory")
	parser.add_argument('--ext', help='optional file extentions', action='append', default=[])

	args = parser.parse_args()

	print "in {} looking for files of {}".format(args.top, args.ext)

	needles = ["lib/jquery-1.7.1.min.js", "lib/jquery-1.4.4.min.js"]

	for fpath in find_files(args.top, args.ext):
		with open(fpath) as f:
			for line in f:
				for needle in needles:
					if -1 < line.find(needle):
						print "found {}".format(line)
						print "in {}".format(fpath)






	