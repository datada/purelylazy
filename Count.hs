import Control.Monad.State
import Control.Monad.Identity

test1 = do
	a <- get
	modify (+ 1)
	b <- get
	return (a, b)

test2 = do
	a <- get
	modify (++ "1")
	b <-get
	return (a, b)

test3 = do
	modify (+ 1)
	lift $ modify (++ "1")
	a <- get
	b <- lift get
	return (a, b)

test5 = do 
	modify (+ 1)
	a <- get 
	lift (print a)
	modify (+ 1)
	b <- get
	lift (print b)

test7 = do
	modify (+ 1)
	lift (modify (++ "1"))
	a <- get
	b <- lift get
	return (a, b)
	

main = do 
	print $ evalState test1 0
	print $ evalState test2 "0"
	print $ runIdentity (evalStateT (evalStateT test3 0) "0")
	evalStateT test5 0
	print $ evalState (evalStateT test7 0) "0"