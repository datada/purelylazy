import System.Random
import Control.Monad.State
import Control.Monad (replicateM)
import Data.List (genericLength)

btwZeroOne :: IO Double
btwZeroOne = do 
	onezero <- (randomRIO (0,1) :: IO Double)
	return onezero

btwUnitSquqre :: IO [Double]
btwUnitSquqre = do 
	pair <- replicateM 2 btwZeroOne
	return pair 

-- pi is equalt to 4 * Area where Area is ratio of random (x y) inside unit circle
main = do
	let total = 1000000
	series <- replicateM total btwUnitSquqre
	print $ 4 * ((genericLength (filter (\(x:y:[]) -> x * x + y * y < 1) series))::Double) / (fromIntegral total)

--{-# LANGUAGE ExtendedDefaultRules, NoMonomorphismRestriction #-}
--import Conduit
--main = do 
--	let cnt = 10000000
--	successes <- sourceRandomN cnt $$ lengthIfc (\(x,y) -> x*x + y*y < 1)
--	print $ successes / cnt * 4