import Control.Monad.State
-- change State to state :: (s -> (a, s)) -> State s a

type Stack = [Int]

{- without Monad
pop (x:xs) = (x, xs)

push a xs = ((), a:xs)

stackManip stack = let ((), stack') = push 3 stack
                       (a, stack'') = pop stack'
                   in pop stack''

> stackManip [5,8,2,1]
-}

pop :: State Stack Int
pop = state (\(x:xs) -> (x, xs))

push :: Int -> State Stack ()
push a = state (\xs -> ((), a:xs))

stackManip :: State Stack Int
stackManip = do 
	push 3
	a <- pop
	pop

main = do 
	print $ runState stackManip [5,8,2,1]