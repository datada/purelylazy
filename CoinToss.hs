import System.Random
import Control.Monad.State
import Control.Monad (replicateM)

-- "functional"
threeToss :: StdGen -> (Bool, Bool, Bool)
threeToss seed = let (coin1, seed1) = random seed 
                     (coin2, seed2) = random seed1
                     (coin3, seed3) = random seed2
                 in (coin1, coin2, coin3)

-- "monadic"
randomSt :: (RandomGen g, Random a) => State g a
randomSt = state random 

threeCoins :: State StdGen (Bool, Bool, Bool)
threeCoins = do
	a <- randomSt
	b <- randomSt
	c <- randomSt
	return (a, b, c)

flipCoin :: IO Bool
flipCoin = do 
	onezero <- (randomRIO (0,1) :: IO Int)
	return (1 == onezero)

main = do
	print $ threeToss (mkStdGen 20398)
	print $ runState threeCoins (mkStdGen 1234567890) 
	series <- replicateM 3 flipCoin
	print $ series