{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}

module Calc where

import ExprT
import Parser
import qualified StackVM as SVM
import StackVM (Program)

eval :: ExprT -> Integer
eval (Lit n) = n
eval (Add e1 e2) = (eval e1) + (eval e2)
eval (Mul e1 e2) = (eval e1) * (eval e2)


evalStr :: String -> Maybe Integer
evalStr s = case (parseExp Lit Add Mul s) of
    Just e -> Just (eval e)
    Nothing -> Nothing


class Expr a where
  lit :: Integer -> a
  add :: a -> a -> a
  mul :: a -> a -> a

instance Expr ExprT where
  lit = Lit . id
  add = \x y -> Add x y
  mul = \x y -> Mul x y

instance Expr Integer where
    lit = id
    add = (+)
    mul = (*)

instance Expr Bool where
    lit = (\i -> if i <= 0 then False else True)
    add = (||)
    mul = (&&)

-- skipping MinMax and Mod7

instance Expr Program where
    lit x = [SVM.PushI x]
    add x y = x ++ y ++ [SVM.Add]  
    mul x y = x ++ y ++ [SVM.Mul]  

-- in ghci, allows
-- reify $ mul (add (lit 2) (lit 3)) (lit 4)
reify :: ExprT -> ExprT
reify = id

testExp :: Expr a => Maybe a 
testExp = parseExp lit add mul "(3 * -4) + 5"

compile :: String -> Maybe Program
compile = parseExp lit add mul 

testExpT    = testExp :: Maybe ExprT
testInteger = testExp :: Maybe Integer
testBool    = testExp :: Maybe Bool
testProg    = testExp :: Maybe Program


main = do 
    print $ eval (Mul (Add (Lit 2) (Lit 3)) (Lit 4)) == 20
    print $ evalStr "(2+3)*4"
    print $ evalStr "2+3*"
    print $ mul (add (lit 2) (lit 3)) (lit 4) == (Mul (Add (Lit 2) (Lit 3)) (Lit 4))
    print $ testExpT
    print $ testInteger
    print $ testBool
    print $ testProg
    print $ compile "(3 * -4) + 5"


