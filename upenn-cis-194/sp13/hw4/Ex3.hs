xor :: [Bool] -> Bool
xor bs = odd $ foldr (\x result  -> if x then result + 1 else result ) 0 bs  

xor' :: [Bool] -> Bool
xor' bs = foldr (/=) False bs

map' :: (a -> b) -> [a] -> [b]
map' f = foldr (\x result -> f x : result) []



main = do
    print $ xor [False, True, False] == True
    print $ xor' [False, True, False] == True
    print $ xor [False, True, False, False, True] == False
    print $ xor' [False, True, False, False, True] == False
    print $ map even [1, 2, 3, 4] == map' even [1, 2, 3, 4]