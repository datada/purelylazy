cartProd :: [a] -> [b] -> [(a, b)]
cartProd xs ys = [(x,y) | x <- xs, y <- ys]


-- take a pass
sieveSundaram :: Integer -> [Integer]
sieveSundaram = undefined


main = do
	print $ cartProd [1,2] ['a', 'b'] == [(1, 'a'),(1, 'b'),(2, 'a'),(2, 'b')]