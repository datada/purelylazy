
fun1 :: [Integer] -> Integer
fun1 [] = 1
fun1 (x:xs)
	| even x = (x - 2) * fun1 xs
	| otherwise = fun1 xs

fun1' :: [Integer] -> Integer
fun1' = product . map (\x -> x - 2) . filter even


-- ???
fun2 :: Integer -> Integer
fun2 1 = 0
fun2 n 
	| even n = n + fun2 (n `div` 2)
	| otherwise = fun2 (3 * n + 1)


main = do
	print $ fun1 [] == fun1' []
	print $ fun1 [1] == fun1' [1]
	print $ fun1 [1, 2, 3] == fun1' [1, 2, 3]
	print $ fun2 1
	print $ fun2 2
	print $ fun2 3