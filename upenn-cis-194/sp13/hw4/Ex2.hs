data Tree a = Leaf | Node Integer (Tree a) a (Tree a) deriving (Show, Eq)


foldTree :: [a] -> Tree a
foldTree = foldr insert Leaf


depth :: Tree a -> Integer
depth Leaf = 0
depth (Node n _ _ _) = n


insert :: a -> Tree a -> Tree a 
insert x Leaf           = Node 0 Leaf x Leaf
insert x (Node n l y r) = 
	let 
	(l', r') = if (depth l) < (depth r) then (insert x l, r) else (l, insert x r) 
	in 
	Node (1 + (max (depth l') (depth r'))) 
         l'
         y
         r'


main = do
	print $ foldTree ""
	print $ foldTree "J"
	print $ foldTree "IJ"
	print $ foldTree "HIJ"
	print $ foldTree "GHIJ"
	print $ foldTree "FGHIJ"
	print $ foldTree "EFGHIJ"
	print $ foldTree "DEFGHIJ"
	print $ foldTree "CDEFGHIJ"
	print $ foldTree "BCDEFGHIJ"
	print $ foldTree "ABCDEFGHIJ"
