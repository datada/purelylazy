import Control.Applicative

type Name = String

data Employee = Employee { name    :: Name
                         , phone   :: String }
                deriving Show

names  = ["Joe", "Sara", "Mae"]
phones = ["555-5555", "123-456-7890", "555-4321"]

employees1 = Employee <$> names <*> phones

main = do 
  print $ employees1
