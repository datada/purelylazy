{- CIS 194 HW 11
   due Monday, 8 April
-}

module SExpr where

import AParser
import Control.Applicative
import Data.Char

------------------------------------------------------------
--  1. Parsing repetitions
------------------------------------------------------------

zeroOrMore :: Parser a -> Parser [a]
zeroOrMore p = oneOrMore p <|> pure []

oneOrMore :: Parser a -> Parser [a]
oneOrMore p = (:) <$> p <*> zeroOrMore p

------------------------------------------------------------
--  2. Utilities
------------------------------------------------------------

spaces :: Parser String
spaces = zeroOrMore (satisfy isSpace)

ident :: Parser String
ident = (:) <$> satisfy isAlpha <*> zeroOrMore (satisfy isAlphaNum)

------------------------------------------------------------
--  3. Parsing S-expressions
------------------------------------------------------------

-- An "identifier" is represented as just a String; however, only
-- those Strings consisting of a letter followed by any number of
-- letters and digits are valid identifiers.
type Ident = String

-- An "atom" is either an integer value or an identifier.
data Atom = N Integer | I Ident
  deriving Show

-- An S-expression is either an atom, or a list of S-expressions.
data SExpr = A Atom
           | Comb [SExpr]
  deriving Show

trim :: Parser a -> Parser a
trim p = spaces *> p <* spaces

paren :: Parser a -> Parser a
paren p = satisfy (=='(') *> p <* satisfy (==')')

parseAtom :: Parser SExpr
parseAtom = A <$> ((N <$> trim posInt) <|> (I <$> trim ident))

parseComb :: Parser SExpr
parseComb = trim (paren (Comb <$> zeroOrMore parseSExpr))

parseSExpr :: Parser SExpr
parseSExpr = parseAtom <|> parseComb



main = do
    print $ runParser (zeroOrMore (satisfy isUpper)) "ABCdefgh"
    print $ runParser (oneOrMore (satisfy isUpper)) "ABCdefgh"
    print $ runParser (zeroOrMore (satisfy isUpper)) "abcdeFGH"
    print $ runParser (oneOrMore (satisfy isUpper)) "abcdeFGH"
    print $ runParser ident "boobar baz"
    print $ runParser ident "foo33fA"
    print $ runParser ident "2bad"
    print $ runParser ident ""
    print $ runParser (trim (char 'a')) "  a  "
    print $ runParser (paren ident) "(one1)"
    print $ runParser parseSExpr "5"
    print $ runParser parseSExpr "foo3"
    print $ runParser parseSExpr "(bar (foo) 3 5 874)"
    print $ runParser parseSExpr "(((labmda x (labmda y (plus x y))) 3) 5)"
    print $ runParser parseSExpr "( lots of ( spaces in ) this ( one ) )"

