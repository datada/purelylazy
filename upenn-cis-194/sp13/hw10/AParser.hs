{- CIS 194 HW 10
   due Monday, 1 April
-}

module AParser where

import           Control.Applicative

import           Data.Char

-- A parser for a value of type a is a function which takes a String
-- represnting the input to be parsed, and succeeds or fails; if it
-- succeeds, it returns the parsed value along with the remainder of
-- the input.
newtype Parser a = Parser { runParser :: String -> Maybe (a, String) }

-- For example, 'satisfy' takes a predicate on Char, and constructs a
-- parser which succeeds only if it sees a Char that satisfies the
-- predicate (which it then returns).  If it encounters a Char that
-- does not satisfy the predicate (or an empty input), it fails.
satisfy :: (Char -> Bool) -> Parser Char
satisfy p = Parser f
  where
    f [] = Nothing    -- fail on the empty input
    f (x:xs)          -- check if x satisfies the predicate
                        -- if so, return x along with the remainder
                        -- of the input (that is, xs)
        | p x       = Just (x, xs)
        | otherwise = Nothing  -- otherwise, fail

-- Using satisfy, we can define the parser 'char c' which expects to
-- see exactly the character c, and fails otherwise.
char :: Char -> Parser Char
char c = satisfy (== c)

{- For example:

*Parser> runParser (satisfy isUpper) "ABC"
Just ('A',"BC")
*Parser> runParser (satisfy isUpper) "abc"
Nothing
*Parser> runParser (char 'x') "xyz"
Just ('x',"yz")

-}

-- For convenience, we've also provided a parser for positive
-- integers.
posInt :: Parser Integer
posInt = Parser f
  where
    f xs
      | null ns   = Nothing
      | otherwise = Just (read ns, rest)
      where (ns, rest) = span isDigit xs

------------------------------------------------------------
-- Your code goes below here
------------------------------------------------------------

instance Functor Parser where
  fmap f p = Parser (\s -> case runParser p s of
                            Nothing -> Nothing
                            Just (v, s') -> Just (f v, s'))

instance Applicative Parser where
  pure a = Parser (\s -> Just (a, s))
  p1 <*> p2 = Parser (\s -> case runParser p1 s of
                             Nothing -> Nothing
                             Just (f, s') -> runParser (f <$> p2) s')

abParser' :: Parser (Char, Char)
abParser' = Parser (\s -> case runParser (char 'a') s of
                           Nothing -> Nothing
                           Just (c, s') -> case runParser (char 'b') s' of
                                             Nothing -> Nothing
                                             Just (c', s'') -> Just ((c, c'), s''))

abParser :: Parser (Char, Char)
abParser = (,) <$> (char 'a') <*> (char 'b')
 
-- pure () <$> abParser does work but why?
abParser_ :: Parser ()
abParser_ =  (const ()) <$> abParser

intPair :: Parser [Integer]
intPair = (\i _ k -> [i,k]) <$> posInt <*> char ' ' <*> posInt

instance Alternative Parser where
  empty = Parser (\s -> Nothing)
  p1 <|> p2 = Parser (\s -> case runParser p1 s of
                              Nothing -> runParser p2 s
                              Just (v, s') -> Just (v, s')) 

intOrUppercase :: Parser ()
intOrUppercase = (pure () <$> posInt) <|> (pure () <$> (satisfy isUpper))


main = do 
  print $ runParser (satisfy isUpper) "ABC"
  print $ runParser (satisfy isUpper) "abc"
  print $ runParser (char 'x') "xyz"
  print $ runParser posInt "123"
  print $ runParser abParser "abc"
  print $ runParser abParser_ "abc"
  print $ runParser abParser_ "cba"
  print $ runParser intPair "12 34"
  print $ runParser intOrUppercase "342abcd"
  print $ runParser intOrUppercase "XYZ"
  print $ runParser intOrUppercase "foo"



