{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Risk where

import Control.Monad.Random
import Data.List (sort, reverse)

------------------------------------------------------------
-- Die values

newtype DieValue = DV { unDV :: Int } 
  deriving (Eq, Ord, Show, Num)

first :: (a -> b) -> (a, c) -> (b, c)
first f (a, c) = (f a, c)

instance Random DieValue where
  random           = first DV . randomR (1,6)
  randomR (low,hi) = first DV . randomR (max 1 (unDV low), min 6 (unDV hi))

die :: Rand StdGen DieValue
die = getRandom

dice :: Int -> Rand StdGen [DieValue]
dice n = sequence (replicate n die)


------------------------------------------------------------
-- Risk

type Army = Int

data Battlefield = Battlefield { attackers :: Army, defenders :: Army }

instance Show Battlefield where
    show bf = (show (attackers bf)) ++ " attack " ++ (show (defenders bf))

attacksPossible :: Battlefield -> Int
attacksPossible bf = minimum [3,  (attackers bf) -1]

defensesPossible :: Battlefield -> Int 
defensesPossible bf = minimum [2, (defenders bf)] 

-- equal number of moves are expected
casualties :: [Int] -> [Int] -> (Int, Int)
casualties [] [] = (0,0)
casualties (0:as) (0:ds) = casualties as ds 
casualties (a:as) (d:ds) = let (a', d') = casualties as ds in if a <= d then (a' + 1, d') else (a', d' + 1)

decide :: [Int] -> [Int] -> Battlefield -> Battlefield
decide aRolls dRolls bf = let 
        attacks = (reverse (sort (take (attacksPossible bf) aRolls))) ++ (take (3 - (attacksPossible bf)) [0,0,0])
        defenses = (reverse (sort (take (defensesPossible bf) dRolls))) ++ (take (3 - (defensesPossible bf)) [0,0,0])
        (aDeaths, dDeaths) = casualties attacks defenses
        in Battlefield ((attackers bf) - aDeaths) ((defenders bf) - dDeaths)

battle :: Battlefield -> Rand StdGen Battlefield
battle bf = do
    attacks <- dice 3
    defenses <- dice 3
    return (decide (map unDV attacks) (map unDV defenses) bf)

invade :: Battlefield -> Rand StdGen Battlefield
invade bf
  | ceaseFire bf = return bf 
  | otherwise = battle bf >>= invade
  where 
    ceaseFire bf = (attackers bf) < 2 || (defenders bf) < 1

successProb :: Battlefield -> Rand StdGen Double
successProb bf = do
    battles <- (sequence (replicate 1000 (invade bf)))
    return  ((fromIntegral (length (filter (\bf -> (defenders bf) < 1) battles))) / 1000.0)

main = do
    print (Battlefield 10 10)
    evalRandIO die >>= print . unDV
    evalRandIO (dice 3) >>= print . (map unDV)
    evalRandIO (dice 2) >>= print . (map unDV)
    evalRandIO (battle (Battlefield 10 10)) >>= print
    evalRandIO (invade (Battlefield 10 10)) >>= print
    evalRandIO (successProb (Battlefield 10 10)) >>= print
