module Golf where

-- for histo
import Data.List
import qualified Data.Map as Map

-- ["a", "b", "a", "c", "b", "b"] is sorted
-- ["a", "a", "b", "b", "b", "c"] is group'd
-- [["a", "a"] ["b", "b", "b"] ["c"]] then changed to
-- [("a", 1), ("b", 3), ("c", 1)] then to "dict"
histo :: Ord a => [a] -> [(a, Int)]
histo xs = [(head l, length l) | l <- group (sort xs)]


-- [(1, 4), (3,1)] -> [(0, 0),(1, 4), (2, 0), (3,1), ... (9,0)]
fillInZeros :: [(Int, Int)] -> [(Int, Int)]
fillInZeros x = Map.toList $ Map.union (Map.fromList x)  (Map.fromList (map (\n -> (n, 0)) [0..9])) 

-- [(0, 0),(1, 4), (2, 0), (3,1), ... (9,0)] -> 4
maxVal :: Ord a =>  [(a, Int)] -> Int 
maxVal xs = maximum (map (\(k,v) -> v) xs)


-- horizontal style
histogram' :: [Int] -> [String]
histogram' ns = map (\(k, v) -> (show k) ++ "=" ++ (replicate v '*') ++ (drop v blanks)) hs
	where 
    hs = fillInZeros (histo ns)
    blanks = replicate (maxVal hs) ' '

-- vertical style
histogram :: [Int] -> [String]
histogram ns = transpose $ map reverse (histogram' ns)



main :: IO()
main = do 
  putStrLn (intercalate "\n" (histogram' [1,1,1,5]))
  putStrLn (intercalate "\n" (histogram [1,4,5,4,6,6,3,4,2,4,9]))