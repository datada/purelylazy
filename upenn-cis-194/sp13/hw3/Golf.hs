module Golf where

-- for histo
import Data.List
import qualified Data.Map as Map


every :: Int -> [a] -> [a]
every n xs = case drop (n - 1) xs of
  (y:ys) -> y : every n ys
  [] -> []
  
skips :: [a] -> [[a]]
skips = skyps 1

skyps :: Int -> [a] -> [[a]]
skyps _ [] = []
skyps n (x:xs) = (x:every n xs):skyps (n+1) xs


localMaxima :: [Integer] -> [Integer]
localMaxima [] = []
localMaxima (x:[]) = []
localMaxima (x:y:[]) = []
localMaxima (x:y:z:s) = if (x < y) && (y > z) then y:localMaxima (z:s) else localMaxima (y:z:s)


windowsOf :: Int -> [Integer] -> [[Integer]]
windowsOf n xs
  | length xs < n = []
  | otherwise     = (take n xs):(windowsOf n (drop 1 xs))

localMaxima' :: [Integer] -> [Integer]
localMaxima' xs = map (\(a:b:c:[]) -> b) $ filter (\(a:b:c:[]) -> a < b && c < b) (windowsOf 3 xs)

-- ["a", "b", "a", "c", "b", "b"] is sorted
-- ["a", "a", "b", "b", "b", "c"] is group'd
-- [["a", "a"] ["b", "b", "b"] ["c"]] then changed to
-- [("a", 1), ("b", 3), ("c", 1)]
histo :: Ord a => [a] -> [(a, Int)]
histo xs = [(head l, length l) | l <- group (sort xs)]


-- [(1, 4), (3,1)] -> [(0, 0),(1, 4), (2, 0), (3,1), ... (9,0)]
fillInZeros :: [(Int, Int)] -> [(Int, Int)]
fillInZeros x = Map.toList $ Map.union (Map.fromList x)  (Map.fromList (map (\n -> (n, 0)) [0..9])) 

-- [(0, 0),(1, 4), (2, 0), (3,1), ... (9,0)] -> 4
maxVal :: Ord a =>  [(a, Int)] -> Int 
maxVal xs = maximum (map (\(k,v) -> v) xs)

barsFromHisto :: Ord a =>  [(a, Int)] -> Int -> [String]
barsFromHisto xs n
	| n == 0    =  ["=========="] ++ ["0123456789"]
	| otherwise = (map (\(x,y) -> if y == n then '*' else ' ') xs):barsFromHisto (map (\(x,y) -> if y == n then (x, y-1) else (x,y)) xs) (n-1) 


histogram :: [Int] -> String
histogram ns = intercalate "\n" (barsFromHisto hs (maxVal hs))
	where hs = fillInZeros (histo ns)


main :: IO()
main = do 
  print $ skips "ABCD"            == ["ABCD", "BD", "C", "D"]
  print $ skips "hello!"          == ["hello!", "el!", "l!", "l", "o", "!"]
  print $ skips [1]               == [[1]]
  print $ skips [True,False]      == [[True,False], [False]]
  print $ localMaxima [2,9,5,6,1] == [9,6]
  print $ localMaxima [2,3,4,1,5] == [4]
  print $ localMaxima [1,2,3,4,5] == []
  print $ localMaxima [2,9,5,6,1] == [9,6]
  print $ localMaxima [2,3,4,1,5] == [4]
  print $ localMaxima [1,2,3,4,5] == []
  putStr (histogram [3,5])
  putStr "\n"
  putStr (histogram [1,4,5,4,6,6,3,4,2,4,9])
  --print $ maxVal $ fillInZeros (histo [1,4,5,4,6,6,3,4,2,4,9])
  --print $ fillInZeros (histo [1,4,5,4,6,6,3,4,2,4,9])
  --print $ map (\(x,y) -> if y == 4 then '*' else ' ') (fillInZeros (histo [1,4,5,4,6,6,3,4,2,4,9]))
  --print $ map (\(x,y) -> if y == 4 then (x, y-1) else (x,y)) (fillInZeros (histo [1,4,5,4,6,6,3,4,2,4,9]))