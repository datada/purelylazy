module Party where

import Data.Monoid
import Data.Tree
import Data.List (intercalate)

import Employee

glCons :: Employee -> GuestList -> GuestList
glCons employee (GL employees fun) = GL (employee:employees) (fun + (empFun employee)) 

instance  Monoid GuestList where
    mempty = GL [] 0
    mappend (GL es f) (GL es' f') = GL (es ++ es') (f + f')

-- see Intance Ord GuestList
moreFun :: GuestList -> GuestList -> GuestList
moreFun = max

treeFold :: (a -> [b] -> b) -> Tree a -> b
treeFold f (Node label children) = f label (fmap (treeFold f) children)

treeSize :: Tree a -> Int 
treeSize = treeFold (\a bs -> 1 + sum bs)

-- hard to word this one
-- following the algorithm, which is not obvious
nextLevel :: Employee -> [(GuestList, GuestList)] -> (GuestList, GuestList)
nextLevel boss gls = let
    (bestLGL, bestRGL) = mconcat $ map (\(lgl, rgl) -> (rgl, moreFun lgl rgl)) gls 
    in (glCons boss bestLGL, bestRGL)

maxFun :: Tree Employee -> GuestList
maxFun = uncurry max . treeFold nextLevel

test = do
    print $ glCons Emp {empName = "Francis Deluzain", empFun = 90} (GL [ Emp {empName = "Jon Do", empFun = 5} ] 5) 
    print $ treeSize testCompany
    print $ maxFun testCompany
    print $ maxFun testCompany2

glStr :: GuestList -> String
glStr (GL es f) = "Total fun: " ++ (show f) ++ "\n" ++ (intercalate "\n" (map empName es))

main = (readFile "company.txt") >>= (\cs -> putStrLn $ glStr (maxFun (read cs)))

