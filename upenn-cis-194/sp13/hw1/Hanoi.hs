type Peg = String
type Move = (Peg, Peg)

hanoi :: Integer -> Peg -> Peg -> Peg -> [Move]
hanoi 0 _ _ _ = []
hanoi n src des aux = hanoi (n - 1) src aux des ++ [(src, des)] ++ hanoi (n - 1) aux des src


main = do
	print $ hanoi 2 "a" "b" "c" == [("a", "c"), ("a", "b"), ("c", "b")]