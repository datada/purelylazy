-- http://www.seas.upenn.edu/~cis194/spring13/hw/01-intro.pdf
import Control.Exception (assert)

lastDigit :: Integer -> Integer
lastDigit = (`mod` 10)

butLastDigit :: Integer -> Integer
butLastDigit = (`div` 10)

toDigits :: Integer -> [Integer]
toDigits = reverse . toDigitsRev

toDigitsRev :: Integer -> [Integer]
toDigitsRev n 
  | n <= 0  = []
  | otherwise = lastDigit n : toDigitsRev (butLastDigit n)

-- works on the reversed input
doubleEveryOther' :: [Integer] -> [Integer]
doubleEveryOther' [] = []
doubleEveryOther' [x] = [x]
doubleEveryOther' (x:y:xs) = x : (y * 2) : doubleEveryOther' xs

doubleEveryOther :: [Integer] -> [Integer]
doubleEveryOther xs = reverse (doubleEveryOther' (reverse xs))

sumDigits :: [Integer] -> Integer
sumDigits [] = 0
sumDigits xs = sum(xs >>= toDigitsRev)

validate :: Integer -> Bool
validate cc = (sumDigits . doubleEveryOther' . toDigitsRev) cc `mod` 10 == 0

main = do 
  putStrLn $ assert([1,2,3,4] == toDigits 1234) "."
  putStrLn $ assert ([] == doubleEveryOther []) "."
  putStrLn $ assert ([1] == doubleEveryOther [1]) "."
  putStrLn $ assert ([2,2] == doubleEveryOther [1,2]) "."
  putStrLn $ assert ([1,4,3] == doubleEveryOther [1,2,3]) "."
  putStrLn $ assert ([2,2,6,4] == doubleEveryOther [1,2,3,4]) "."
  putStrLn $ assert ([16,7,12,5] == doubleEveryOther [8,7,6,5]) "."
  putStrLn $ assert (22 == sumDigits [16,7,12,5]) "."
  putStrLn $ assert (validate 4012888888881881) "."
  putStrLn $ assert (not (validate 4012888888881882)) "."
  
