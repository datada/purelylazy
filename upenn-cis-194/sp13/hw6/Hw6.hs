fib :: Integer -> Integer
fib 0 = 0
fib 1 = 1
fib n = fib (n - 1) + fib (n - 2)

fibIter :: Integer -> Integer -> Integer -> Integer
fibIter a b 0 = a
fibIter a b n = fibIter b (b + a) (n - 1)

fib' :: Integer -> Integer
fib' n = fibIter 0 1 n

fibs1 :: [Integer]
fibs1 = map fib [0..]

-- interesting!
fibs2 :: [Integer]
fibs2 = scanl (+) 0 (1:fibs2)

fibs3 :: [Integer]
fibs3 = 0 : 1 : zipWith (+) fibs3 (drop 1 fibs3)

fibs4 :: [Integer]
fibs4 = 0 : 1 : [x + y | (x, y) <- zip fibs4 (tail fibs4)]

data Stream a = Cons a (Stream a)

streamToList :: Stream a -> [a]
streamToList (Cons a as) = a : streamToList as

instance Show a => Show (Stream a) where
    show = show . (take 20) . streamToList

streamRepeat :: a -> Stream a
streamRepeat a = Cons a (streamRepeat a)

streamMap :: (a -> b) -> Stream a -> Stream b
streamMap f (Cons a as) = Cons (f a) (streamMap f as)

streamFromSeed :: (a -> a) -> a -> Stream a
streamFromSeed f a = Cons a (streamFromSeed f (f a))

nats :: Stream Integer
nats = streamFromSeed (\x -> x + 1) 1

nats1 = 1:(map (+1) nats1)

nats2 = iterate (+1) 1

-- pass
ruler :: Stream Integer
ruler = undefined


main = do
    print $ fib 0
    print $ fib 1
    print $ fib 2
    print $ take 20 fibs1
    print $ take 20 fibs2
    print $ take 20 fibs3
    print $ take 20 fibs4
    print $ nats
    print $ take 20 nats1
    print $ take 20 nats2
