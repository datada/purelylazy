{-# LANGUAGE FlexibleInstances #-}
module JoinList where

import Control.Applicative
import Data.Monoid

import Editor
import Buffer
import Sized
import Scrabble

data JoinList m a = Empty
                  | Single m a
                  | Append m (JoinList m a) (JoinList m a)
                  deriving (Eq, Show)

(+++) :: Monoid m => JoinList m a -> JoinList m a -> JoinList m a 
(+++) x y = Append ((tag x) <> (tag y)) x y

tag :: Monoid m => JoinList m a -> m 
tag Empty           = mempty 
tag (Single m _)    = m 
tag (Append m _ _ ) = m 

(!!?) :: [a] -> Int -> Maybe a
[]     !!? _         = Nothing
_      !!? i | i < 0 = Nothing
(x:xs) !!? 0         = Just x
(x:xs) !!? i         = xs !!? (i - 1)

jlToList :: JoinList m a -> [a]
jlToList Empty = []
jlToList (Single _ a) = [a]
jlToList (Append _ l r) = jlToList l ++ jlToList r

indexJ :: (Sized b, Monoid b) => Int -> JoinList b a -> Maybe a 
indexJ i = indexJ' (i + 1)

indexJ' :: (Sized b, Monoid b) => Int -> JoinList b a -> Maybe a 
indexJ' _ Empty = Nothing
indexJ' i _ | i <= 0 = Nothing
indexJ' i (Single m x)
    | i == (getSize (size m)) = Just x 
    | otherwise = Nothing 
indexJ' i (Append m l r)
    | i <= (getSize (size (tag l))) = indexJ' i l
    | otherwise                     = indexJ' (i - (getSize (size (tag l)))) r

dropJ :: (Sized b, Monoid b) => Int -> JoinList b a -> JoinList b a
dropJ _ Empty      = Empty
dropJ 0 l = l
dropJ n (Single m v) = Empty
dropJ n (Append m l r)
    | n < (getSize (size (tag l))) = (dropJ n l) +++ r
    | otherwise             = dropJ (n - (getSize (size (tag l)))) r

takeJ ::(Sized b, Monoid b) => Int -> JoinList b a -> JoinList b a 
takeJ _ Empty = Empty
takeJ 0 _ = Empty
takeJ _ (Single m v) = Single m v
takeJ n (Append m l r)
    | n < (getSize (size (tag l)))  = takeJ n l 
    | n == (getSize (size (tag l))) = l
    | otherwise                     = l +++ takeJ (n - (getSize (size (tag l)))) r

scoreLine :: String -> JoinList Score String
scoreLine = foldr (+++) Empty . map (\w -> Single (scoreString w) w) . words

-- mimic StringBuffer.hs
instance  Buffer (JoinList (Score, Size) String) where
  toString     = concat . jlToList
  fromString   = foldr (+++) Empty . map (\ln -> Single (scoreString ln, (Size 1)) ln) . lines
  line         = indexJ
  replaceLine n l b = takeJ (n - 1) b +++ Single (scoreString l, (Size 1)) l +++ dropJ (n + 1) b
  numLines     = getSize . size . snd . tag
  value        = getScore . fst . tag


test = do
    print $ (Single (Size 1) 'e') +++ (Single (Size 1) 'f')
    print $ jlToList ((Single (Size 1) 'e') +++ (Single (Size 1) 'f'))
    print $ indexJ 0 ((Single (Size 1) 'e') +++ (Single (Size 1) 'f'))
    print $ indexJ 1 ((Single (Size 1) 'e') +++ (Single (Size 1) 'f'))
    print $ dropJ 1 ((Single (Size 1) 'e') +++ (Single (Size 1) 'f'))
    print $ dropJ 2 ((Single (Size 1) 'e') +++ (Single (Size 1) 'f'))
    print $ takeJ 0 ((Single (Size 1) 'e') +++ (Single (Size 1) 'f'))
    print $ takeJ 1 ((Single (Size 1) 'e') +++ (Single (Size 1) 'f'))
    print $ takeJ 2 ((Single (Size 1) 'e') +++ (Single (Size 1) 'f'))
    print $ scoreLine "yay " +++ scoreLine "haskell!"
    print $ (fromString "yay" :: JoinList (Score, Size) String)

main = runEditor editor (Empty :: JoinList (Score, Size) String)


