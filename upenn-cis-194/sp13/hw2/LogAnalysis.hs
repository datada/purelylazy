{-# OPTIONS_GHC -Wall #-}
module LogAnalysis where
import Log

parseMessage :: String -> LogMessage
parseMessage x = case words x of
           ("E":n:ts:ss) -> LogMessage (Error (read n)) (read ts) (unwords ss)
           ("I":ts:ss)   -> LogMessage Info (read ts) (unwords ss)
           ("W":ts:ss)   -> LogMessage Warning (read ts) (unwords ss)
           _             -> Unknown x

parse :: String -> [LogMessage]
parse s = map parseMessage (lines s)


getMsg :: LogMessage -> String
getMsg (LogMessage _ _ s) = s
getMsg (Unknown s) = s

getTs :: LogMessage -> TimeStamp
getTs (LogMessage _ ts _) = ts
getTs (Unknown _) = 0

insert :: LogMessage -> MessageTree -> MessageTree
insert (Unknown _) mTree = mTree
insert msg Leaf          = Node Leaf msg Leaf
insert lm (Node l lm' r) = if (getTs lm) < (getTs lm')
                           then Node (insert lm l)  lm' r
                           else Node l lm' (insert lm r)

-- foldr takes \a accm ->
build :: [LogMessage] -> MessageTree
build = foldr insert Leaf

inOrder :: MessageTree -> [LogMessage]
inOrder Leaf = []
inOrder (Node l v r) = (inOrder l) ++ [v] ++ (inOrder r)

whatWentWrong :: [LogMessage] -> [String]
whatWentWrong = map getMsg . filter isRelevant . inOrder . build where
  isRelevant (LogMessage (Error n) _ _) = 50 <= n 
  isRelevant _ = False

main :: IO()
main = do
  print $ parseMessage "E 2 562 help help"
  print $ parseMessage "I 29 la la la"
  print $ parseMessage "This is not in the right format"
  ms <- testParse parse 10 "error.log"
  print ms
  putStrLn ""
  putStrLn ""
  testWhatWentWrong parse whatWentWrong "sample.log" >>= print