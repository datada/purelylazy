module HW03 where

data Expression =
    Var String                   -- Variable
  | Val Int                      -- Integer literal
  | Op Expression Bop Expression -- Operation
  deriving (Show, Eq)

-- Binary (2-input) operators
data Bop = 
    Plus     
  | Minus    
  | Times    
  | Divide   
  | Gt
  | Ge       
  | Lt  
  | Le
  | Eql
  deriving (Show, Eq)

data Statement =
    Assign   String     Expression
  | Incr     String
  | If       Expression Statement  Statement
  | While    Expression Statement       
  | For      Statement  Expression Statement Statement
  | Sequence Statement  Statement        
  | Skip
  deriving (Show, Eq)

type State = String -> Int

-- Exercise 1 -----------------------------------------

extend :: State -> String -> Int -> State
extend st key val = \x -> if x == key then val else st x

empty :: State
empty _ = 0

-- Exercise 2 -----------------------------------------

evalE :: State -> Expression -> Int
evalE st (Val i) = i 
evalE st (Var s) = st s 
evalE st (Op e1 op e2) =
  let v1 = evalE st e1
      v2 = evalE st e2
  in case op of
    Plus   -> v1 + v2
    Minus  -> v1 - v2
    Times  -> v1 * v2
    Divide -> v1 `div` v2
    Gt     -> if v1 >  v2 then 1 else 0
    Ge     -> if v1 >= v2 then 1 else 0
    Lt     -> if v1 <  v2 then 1 else 0
    Le     -> if v1 <= v2 then 1 else 0
    Eql    -> if v1 == v2 then 1 else 0


-- Exercise 3 -----------------------------------------

data DietStatement = DAssign String Expression
                   | DIf Expression DietStatement DietStatement
                   | DWhile Expression DietStatement
                   | DSequence DietStatement DietStatement
                   | DSkip
                     deriving (Show, Eq)

desugar :: Statement -> DietStatement
desugar (Assign str e)          = DAssign str e
desugar (Incr str)              = DAssign str (Op (Var str) Plus (Val 1))
desugar (If e s1 s2)            = DIf e (desugar s1) (desugar s2)  
desugar (While e s)             = DWhile e (desugar s)
desugar (For init pred step s)  = DSequence (desugar init) 
                                            (DWhile pred 
                                                    (DSequence (desugar s) 
                                                               (desugar step)))
desugar (Sequence s1 s2)        = DSequence (desugar s1) (desugar s2)     
desugar (Skip)                  = DSkip
            


-- Exercise 4 -----------------------------------------

evalSimple :: State -> DietStatement -> State
evalSimple st (DAssign key e)   = extend st key (evalE st e)
evalSimple st (DIf e s1 s2)     = if (evalE st e) /= 0 then evalSimple st s1 else evalSimple st s2
evalSimple st (DWhile e s)      = if (evalE st e) /= 0 then evalSimple (evalSimple st s) (DWhile e s) else st
evalSimple st (DSequence s1 s2) = evalSimple (evalSimple st s1) s2
evalSimple st (DSkip)           = st

run :: State -> Statement -> State
run st s = evalSimple st (desugar s)

-- Programs -------------------------------------------

slist :: [Statement] -> Statement
slist [] = Skip
slist l  = foldr1 Sequence l

{- Calculate the factorial of the input

   for (Out := 1; In > 0; In := In - 1) {
     Out := In * Out
   }
-}
factorial :: Statement
factorial = For (Assign "Out" (Val 1))
                (Op (Var "In") Gt (Val 0))
                (Assign "In" (Op (Var "In") Minus (Val 1)))
                (Assign "Out" (Op (Var "In") Times (Var "Out")))


{- Calculate the floor of the square root of the input

   B := 0;
   while (A >= B * B) {
     B++
   };
   B := B - 1
-}
squareRoot :: Statement
squareRoot = slist [ Assign "B" (Val 0)
                   , While (Op (Var "A") Ge (Op (Var "B") Times (Var "B")))
                       (Incr "B")
                   , Assign "B" (Op (Var "B") Minus (Val 1))
                   ]

{- Calculate the nth Fibonacci number

   F0 := 1;
   F1 := 1;
   if (In == 0) {
     Out := F0
   } else {
     if (In == 1) {
       Out := F1
     } else {
       for (C := 2; C <= In; C++) {
         T  := F0 + F1;
         F0 := F1;
         F1 := T;
         Out := T
       }
     }
   }
-}
fibonacci :: Statement
fibonacci = slist [ Assign "F0" (Val 1)
                  , Assign "F1" (Val 1)
                  , If (Op (Var "In") Eql (Val 0))
                       (Assign "Out" (Var "F0"))
                       (If (Op (Var "In") Eql (Val 1))
                           (Assign "Out" (Var "F1"))
                           (For (Assign "C" (Val 2))
                                (Op (Var "C") Le (Var "In"))
                                (Incr "C")
                                (slist
                                 [ Assign "T" (Op (Var "F0") Plus (Var "F1"))
                                 , Assign "F0" (Var "F1")
                                 , Assign "F1" (Var "T")
                                 , Assign "Out" (Var "T")
                                 ])
                           )
                       )
                  ]


main = do
  print $ empty "A" == 0
  print $ (extend empty "A" 5) "A" == 5
  print $ evalE empty (Val 5) == 5
  print $ evalE empty (Op (Val 1) Eql (Val 2)) == 0
  print $ desugar (Incr "A") == DAssign "A" (Op (Var "A") Plus (Val 1))
  print $ let s = run (extend empty "In" 4) factorial in s "Out" == 24
  print $ let s = run (extend empty "A" 4) squareRoot in s "B" == 2
  print $ let s = run (extend empty "In" 0) fibonacci in s "Out" == 0 --seems wrong! but explained in http://www.seas.upenn.edu/~cis194/spring13/hw/06-laziness.pdf
