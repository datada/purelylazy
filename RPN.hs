import Data.List
import Control.Monad

solveRPN :: String -> Maybe Double
solveRPN st = do 
	[result] <- foldM foldingFunction [] (words st)
	return result

readMaybe :: (Read a) => String -> Maybe a
readMaybe st = case reads st of
	[(x, "")] -> Just x
	_       -> Nothing

foldingFunction :: [Double] -> String -> Maybe [Double]
foldingFunction (x:y:ys) "*" = return ((x * y):ys)
foldingFunction (x:y:ys) "+" = return ((x + y):ys)
foldingFunction (x:y:ys) "-" = return ((y - x):ys)
foldingFunction xs numStr    = liftM (:xs) (readMaybe numStr)


main = do 
	print $ solveRPN "1 2 * 4 + 5 *"
	print $ solveRPN "1 2 * 4" 