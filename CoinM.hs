import Data.Ratio
import Data.Char (toUpper)

newtype Prob a = Prob { getProb :: [(a, Rational)]} deriving Show

instance Functor Prob where
	fmap f (Prob xs) = Prob $ map (\(x, p) -> (f x, p)) xs 

thisSituation :: Prob (Prob Char)
thisSituation = Prob [ (Prob [('a', 1%2), ('b', 1%2)], 1%4)
                     , (Prob [('c', 1%2), ('d', 1%2)], 3%4)
                     ]

flatten :: Prob (Prob a) -> Prob a
flatten (Prob xs) = Prob $ concat $ map mulAll xs
	where mulAll (Prob innerxs, p) = map (\(x, r) -> (x, p * r)) innerxs

instance  Monad Prob where
	return x = Prob [(x, 1%1)]
	m >>= f = flatten (fmap f m)
	fail _ = Prob []

data Coin = Heads | Tails deriving (Show, Eq)

coin :: Prob Coin 
coin = Prob [(Heads, 1%2), (Tails, 1%2)]

flipThree :: Prob [Coin]
flipThree = do
	a <- coin 
	b <- coin 
	c <- coin
	return [a, b, c]

allTails :: Prob Bool
allTails = do
	xs <- flipThree
	return (all (== Tails) xs)

main = do
	print $ thisSituation
	print $ fmap toUpper (Prob [('a', 1%2), ('b', 1%2)])
	print $ getProb flipThree
	print $ getProb allTails
