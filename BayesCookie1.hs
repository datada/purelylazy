import Data.Ratio
import Control.Exception (assert)

-- P(H | D) = P(H)P(D | H) / P(D)

-- Bowl 1 contains 30 Vanillar and 10 Chocolate cookies.
-- Bowl 2 contains 20 each.

-- Q: Given a cookie, which Bowl did it come from?

-- prior Belief/Hypothesis, aka P(H)
-- given no info, equally likely
cookieBowls :: [(String, Rational)]
cookieBowls = [("Bowl 1", 1%2), ("Bowl 2", 1%2)]

-- "dividing by p(D)" by normalizing instead of calculating P(D) using total probablility
normalize :: [(String, Rational)] -> [(String, Rational)]
normalize xs = let total = sum (map (\(_, p) -> p) xs)
               in map (\(x, p) -> (x, p / total)) xs

-- element by element *
-- each H is updated with P(D|H)
-- assert x == y ie H's are aligned
mult :: [(String, Rational)] -> [(String, Rational)] -> [(String, Rational)]
mult xs ys = zipWith (\(x, p) (y, q) -> assert (x == y) (x, p * q)) xs ys

main = do
	-- We pick a cookie and it is Vanilla.
	-- We update with P(V | B1) = 3/4 and P(V | B2) = 1/2 and normalize 
	print $ normalize (mult cookieBowls [("Bowl 1", 3%4), ("Bowl 2", 1%2)])


