import Data.Ratio
import Bayes
-- P(H | D) = P(H)P(D | H) / P(D)

-- Bowl 1 contains 30 Vanillar and 10 Chocolate cookies.
-- Bowl 2 contains 20 each.

-- Q: Picked a cookie. It is Vanilla. What's the prob that it si from Bowl i?

data Hypothesis = Bowl1 | Bowl2 deriving (Show, Eq)

data Evidence = Vanilla | Chocolate deriving (Show, Eq)

-- prior Belief/Hypothesis, aka P(H)
-- given no info, equally likely
prior :: [(Hypothesis, Rational)]
prior = [ (Bowl1, 1%2)
        , (Bowl2, 1%2)]

-- P(D | H)
likelyhood :: Evidence -> [(Hypothesis, Rational)]
likelyhood Vanilla   = [(Bowl1, 3%4), (Bowl2, 1%2)]
likelyhood Chocolate = [(Bowl1, 1%4), (Bowl2, 1%2)]


main = do
	-- We pick a cookie and it is Vanilla.
	-- We update with P(V | B1) = 3/4 and P(V | B2) = 1/2 and normalize 
	print $ normalize (mult prior
	                        (likelyhood Vanilla))
	print $ posterior prior likelyhood Vanilla

