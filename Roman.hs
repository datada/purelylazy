-- http://dalelane.co.uk/blog/?p=3244
-- Homework done in python

romanToNumber :: String -> Int
romanToNumber ('I':'V':cs) = 4 + (romanToNumber cs)
romanToNumber ('I':'X':cs) = 9 + (romanToNumber cs)
romanToNumber ('X':'L':cs) = 40 + (romanToNumber cs)
romanToNumber ('X':'C':cs) = 90 + (romanToNumber cs)
romanToNumber ('C':'D':cs) = 400 + (romanToNumber cs)
romanToNumber ('C':'M':cs) = 900 + (romanToNumber cs)
romanToNumber ('I':cs)     = 1 + (romanToNumber cs)
romanToNumber ('V':cs)     = 5 + (romanToNumber cs)
romanToNumber ('X':cs)     = 10 + (romanToNumber cs)
romanToNumber ('L':cs)     = 50 + (romanToNumber cs)
romanToNumber ('C':cs)     = 100 + (romanToNumber cs)
romanToNumber ('D':cs)     = 500 + (romanToNumber cs)
romanToNumber ('M':cs)     = 1000 + (romanToNumber cs)
romanToNumber _            = 0


main = do
	print $ 2015 == romanToNumber "MMXV"
	print $ 1904 == romanToNumber "MCMIV"
	print $ 1990 == romanToNumber "MCMXC"
	print $ 2014 == romanToNumber "MMXIV"