import Control.Applicative
import Control.Monad
import Control.Monad.Writer

-- ch4 syntax

-- pattern matching vs guard vs case
data Move = Rock | Paper | Scissors deriving (Eq, Read, Show, Enum, Bounded)
data Outcome = Lose | Tie | Win deriving (Show, Eq, Ord)

outcome :: Move -> Move -> Outcome 
outcome Rock Scissors = Win
outcome Paper Rock = Win
outcome Scissors Paper = Win
outcome us them | us == them = Tie
                | otherwise = Lose

parseMove :: String -> Maybe Move 
parseMove str = case reads str of
    [(m, rest)] | ok rest -> Just m 
    _                     -> Nothing
    where ok = all (`elem` " \r \n")

-- let vs where
slope (x1, y1) (x2, y2) = let dy = y2 - y1
                              dx = x2 - x1
                          in dy/dx

fact n0 = loop 1 n0 where
    loop acc n | 1 < n = loop (acc * n) (n - 1)
               | otherwise = acc

-- let does not work here
f x y | z < y = "<"
      | y == z = "=="
      | y < z = ">"
      where z = x * x


-- ch5
quicksort []     = []
quicksort (x:xs) = quicksort [y | y <- xs, y < x] ++ [x] ++ quicksort [y | y <- xs, x <= y]


-- ch6 higher order
-- foldr + 0 [1,2] AKA 1 + (2 + 0)
-- foldr :: (a -> b -> b) -> b -> [a] -> b
-- foldr f z [] = z
-- foldr f z (x:xs) = f x (foldr f z xs)

-- foldl + 0 [1,2,] AKA (0 + 1) + 2
-- foldl :: (a -> b -> a) -> a -> [b] -> a
-- foldl f z [] = z
-- foldl f z (x:xs) = fold f (f z x) xs

-- filter p xs = foldr step [] xs where 
--   step x ys | p x = x:ys
--             | otherwise = ys

-- foldl f z xs = foldr stepp id xs z where step x g a = g (f a x)


-- ch7 module
-- ghci> :m + Data.List Data.Map Data.Set
-- import Data.List (nub, sort)
-- import Data.List hiding (nub)
-- import qualified Data.Map as M

-- ch8 types
data Point = Point Float Float deriving (Show)
data Shape = Circle Point Float | Rectangle Point Point deriving (Show)

surface :: Shape -> Float
surface (Circle _ r) = pi * r ^ 2
surface (Rectangle (Point x1 y1) (Point x2 y2)) = (abs (x2 - x1)) * (abs (y2 - y1))

-- ch 12, 13 Monads
type Birds = Int
type Pole = (Birds, Birds)

landLeft :: Birds -> Pole -> Maybe Pole
landLeft n (left, right) | abs ((left + n) - right) < 4 = Just (left + 4, right)
                         | otherwise                    = Nothing

landRight :: Birds -> Pole -> Maybe Pole 
landRight n (left, right) | abs (left - (right + n)) < 4 = Just (left, right + 4)
                          | otherwise                    = Nothing

mygcd :: Int -> Int -> Writer [String] Int
mygcd a b | b == 0 = do
            tell ["Finished with " ++ show a]
            return a
        | otherwise = do 
            result <- mygcd b (a `mod` b)
            tell [show a ++ " mod " ++ show b ++ " = " ++ show (a `mod` b)]
            return result


-- list stuff
listOfTuples :: [(Int, Char)]
listOfTuples = do
    n <- [1,2]
    ch <- "ab"
    return (n, ch)

sevensOnly :: [Int]
sevensOnly = do 
    x <- [1..50]
    guard ('7' `elem` show x)
    return x

powerset :: [a] -> [[a]]
powerset xs = filterM (\x -> [True, False]) xs


main = do
    -- ch2
    print $ [(a,b,c) | a <- [1..10], b <- [1..10], c <- [1..10], a^2 + b^2 == c^2, a + b + c == 24]
    -- ch3
    print $ (read "4" :: Int)
    print $ (read "4" :: Integer)
    print $ (read "4" :: Float)
    print $ (read "4" :: Double)
    print $ (read "[1,2,3]" :: [Int])
    print $ (read "(3, 'a')" :: (Int, Char))
    print $ fromIntegral (length [1,2,3]) + 3.2
    -- ch4
    print $ parseMove "Rock \r\n"
    print $ outcome <$> (parseMove "Rock") <*> (parseMove "Paper")
    print $ slope (0,0) (1,1)
    print $ fact 25
    print $ quicksort [2,6,3,8,4,6]
    -- ch6
    -- id = foldr (:) []
    print $ id [1,2,3] == (foldr (:) []) [1,2,3]
    -- reverse = foldl (flip (:)) []
    print $ reverse [1,2,3] == (foldl (flip (:)) []) [1,2,3]
    -- map f = foldr ((:) . f) []
    print $ map (*2) [1,2,3] == (\f -> foldr ((:) . f) []) (*2) [1,2,3]
    -- head = foldr (\a bs -> a) undefined
    print $ head [1,2,3] == (foldr (\a bs -> a) undefined) [1,2,3]
    -- last = foldl (\as b -> b) undefined
    print $ last [1,2,3] == (foldl (\as b -> b) undefined) [1,2,3]
    -- ch 8
    print $ surface (Circle (Point 0 0) 2)
    print $ surface (Rectangle (Point 0 0) (Point 1 1))
    -- ch 11
    print $ (,) <$> [1..3] <*> [1..3]
    print $ pure (,) <*> [1..3] <*> [1..3]
    print $ fmap (,) [1..3] <*> [1..3]
    print $ liftM2 (,) [1..3] [1..3]
    -- ch 12
    print $ return (0,0) >>= landRight 2 >>= landLeft 2 >>= landRight 2
    print $ fst $ runWriter (mygcd 8 3)
    mapM_ putStrLn $ snd $ runWriter (mygcd 8 3)
    -- list stuff
    print $ listOfTuples
    print $ [1,2] >>= \n -> "ab" >>= \ch -> return (n, ch)
    print $ [(n, ch) | n <- [1,2], ch <- "ab"]
    print $ sevensOnly
    print $ [1..50] >>= (\x -> guard ('7' `elem` show x) >> return x)
    print $ [x | x <- [1..50], '7' `elem` show x]
    print $ powerset [1,2,3]
